# OpenCodingTool - Uma ferramenta de Análise Qualitativa de Dados Colaborativa via Web

Tabela de conteúdos
=================

   * [Descrição do Projeto](#markdown-header-descricao-do-projeto)
   
   * [Artefatos do Projeto](#markdown-header-artefatos-do-projeto)

## Descrição do Projeto

<p align="center"> Neste trabalho, propomos uma ferramenta que permite a codificação colaborativa entre avaliadores/codificadores para atender uma demanda de arquivos, com base na complexibilidade do serviço. A proposta envolve a ferramenta OpenCodingTool. Cada codificador possui uma permissão de codificação, e cada projeto possui uma complexibilidade. Esses valores são determinados pelo usuário.</p>

## Artefatos do Projeto

<p> Hello World! </p>

Regras de Negócios
-----------------------------
Nº  | Descrição
----| -----------------------
01  | A aplicação deve permitir passar um “código” com status “não avaliado” (0) para o status “em avaliação” (1) à partir do momento que o Codificador selecionar “finalizar codificação” em um determinado arquivo.
02  | A aplicação deve permitir passar um “código” com status “em avaliação” (1) para o status “aceito” (2) à partir do momento que o Avaliador selecionar o “check box” de um determinado código para um determinado arquivo.
03  | A aplicação deve permitir criar um “código” com status “Aceito” (2) diretamente, à partir do momento que o Avaliador ache necessário para um determinado arquivo na página de avaliação.
04  | A aplicação deve permitir passar um “código” com status “Aceito” (1) para “não Aceito” (3), à partir do momento que o Avaliador selecionar o “check box” de um determinado código para um determinado arquivo.
05  | A aplicação deve impedir que um Codificador realize as seguintes opções: “Criar Códigos”, “Finalizar Codificação”, “Editar Código” e “Excluir Código”, logo após o Codificador clicar na opção “Finalizar Codificação”.
06  | A aplicação deve permitir a exportação de arquivo no formato .csv e .pdf, contendo as seguintes informações: “projeto”, “avaliador”, “código”, "categoria"e “arquivo”..
07  | A aplicação deve permitir a categorização por meio do evento "drag and drog", arrasta e solta código na página de categorização do sistema.
08  | A aplicação deve permitir usuários com dois perfis de usuário. O mesmo poderá escolher para qual gerencia deve ser redirecionado após login no sistema.
09  | A aplicação não deve permitir a finalização da criação de projetos sem codificadores adicionados em cada arquivo no sistema.
10  | A aplicação não deve permitir a finalização da categorização, caso ainda falte algum código para ser categorizado no sistema.

 <img alt="casodeuso" title="#Caso de Uso" src="https://github.com/rafaelbtorres/algoritmoMathingUngaroSelecaoTecnico/blob/master/imgs/ucnew2023.jpg?raw=true" />

 <img alt="modelologico" title="#Modelo Lógico" src="https://github.com/rafaelbtorres/algoritmoMathingUngaroSelecaoTecnico/blob/master/imgs/logico.png?raw=true" />
