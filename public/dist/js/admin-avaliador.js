function iniciarCodificacao(idprojeto) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/iniciar',
        async: false,
        data: {
            idprojeto: idprojeto
        },
        success: function (data) {

            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Codificação iniciada com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível iniciar a codificação!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

        },
        complete: function () {
        },
        error: function (error) {
            console.log(error);
        }
    });
//        return false;
}

function pausarCodificacao(idprojeto) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/pausar',
        async: false,
        data: {
            idprojeto: idprojeto
        },
        success: function (data) {

            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Codificação finalizada com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível finalizar a codificação!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

        },
        complete: function () {
        },
        error: function (error) {
            console.log(error);
        }
    });
//        return false;
}

function finalizarAvaliacao(idprojeto) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/finalizar-avaliacao',
        async: false,
        data: {
            idprojeto: idprojeto
        },
        success: function (data) {

            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Avaliação finalizada com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível pausar a codificação!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

        },
        complete: function () {
        },
        error: function (error) {
            console.log(error);
        }
    });
//        return false;
}

function finalizarCategorizacao(idprojeto) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/finalizar-categorizacao',
        async: false,
        data: {
            idprojeto: idprojeto
        },
        success: function (data) {

            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Categorização finalizada com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não é possível finalizar categorização, verique se todos códigos estão categorizados e se não existem categorias sem códigos!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
//                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

        },
        complete: function () {
        },
        error: function (error) {
            console.log(error);
        }
    });
//        return false;
}

function removerProjeto(idprojeto) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/remover',
        async: false,
        data: {
            idprojeto: idprojeto
        },
        success: function (data) {

            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Projeto removido com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }

            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível remover o projeto!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }

        },
        complete: function () {
        },
        error: function (error) {
            console.log(error);
        }
    });
//        return false;
}

//function inativar(idprojeto) {
//
//    bootbox.dialog({
//        message: "Deseja realmente excluir este projeto ?",
//        title: "Mensagem de Alerta!",
//        buttons: {
//            success: {
//                label: "Sim",
//                className: "btn-success",
//                callback: function () {
//                    $.ajax({
//                        type: "POST",
//                        url: baseUrl + 'admin-avaliador/inativar',
//                        async: false,
//                        data: {idprojeto: idprojeto},
//                        success: function (data) {
//                            if (data.flag == 'ok') {
//                                bootbox.dialog({
//                                    message: "Codificação finalizada com sucesso!",
//                                    title: "Aviso",
//                                    buttons: {
//                                        success: {
//                                            label: "OK",
//                                            className: "btn-success",
//                                            callback: function () {
//                                                location.reload();
//                                            }
//                                        }
//                                    }
//                                });
//                            }
//                        },
//                        error: function (msg) {
//                        }
//                    });
//                }
//            },
//            danger: {
//                label: "Não",
//                className: "btn btn-default",
//                callback: function () {
//                    setTimeout(function () {
//                        // that's enough of that
//                        bootbox.hideAll();
//                    }, 3000);
//                }
//            }
//        }
//    });
//}