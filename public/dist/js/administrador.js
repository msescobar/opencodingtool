$(document).ready(function () {
    $('#avaliadorNew').on("submit", function (e)
    {
        e.preventDefault();

        var form = $('#avaliadorNew')[0];
        var data = new FormData(form);

        cadastrarAvaliador(data);
    });

    $('#avaliadorUpdate').on("submit", function (e)
    {
        e.preventDefault();

        var form = $('#avaliadorUpdate')[0];
        var data = new FormData(form);

        editarAvaliador(data);
    });

});

function cadastrarAvaliador(data) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        url: baseUrl + 'administrador/cadastrar',
        async: false,
        data: data,
        success: function (data) {
            if (data.flag === 'ok') {
                bootbox.dialog({
                    message: "Avaliador cadastrado com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function editarUsuario(id)
{
    var login = $("#login" + id).val();
    var email = $("#email" + id).val();
//    alert(descricao);

    $.ajax({
        type: "POST",
        url: baseUrl + 'administrador/editar',
        async: false,
        data: {id: id, login: login, email: email
        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Avaliador atualizado com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function excluirUsuario(id) {

    bootbox.dialog({
        message: "Deseja realmente excluir?",
        title: "Mensagem de Alerta!",
        buttons: {
            success: {
                label: "Sim",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'administrador/excluir/id/' + id,
                        success: function (data) {
                            if (data.flag == 'ok') {
                                bootbox.dialog({
                                    message: "Avaliador excluído com sucesso!",
                                    title: "Mensagem",
                                    buttons: {
                                        success: {
                                            label: "OK",
                                            className: "btn-success",
                                            callback: function () {
                                                location.reload();
                                            }
                                        },
                                    }
                                });
                            }
                        },
                        error: function (msg) {
                            alert(msg);

                        }
                    });
                }
            },
            danger: {
                label: "Não",
                className: "btn-default",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}