$(document).ready(function () {

    $('#codificadorNew').on("submit", function (e)
    {
        e.preventDefault();

        var login = $("#login").val();
        var senha = $("#senha").val();
//        var idprojeto = $("#idprojeto").val();

//        alert("login = " + login + "senha = " + senha + " idgrupo = " + idgrupo);

        $.ajax({
            type: "POST",
            url: baseUrl + 'usuario/cadastrar',
            async: false,
            data: {
                login: login,
                senha: senha
            },
            success: function (data) {
//                alert(data);
                if (data.flag == 'ok') {
                    bootbox.dialog({
                        message: "Codificador cadastrado com sucesso na base de codificadores!\n\
 ATENÇÃO, VOCÊ DEVE INSERIR O CODIFICADOR AO ARQUIVO.",
                        title: "ATENÇÃO",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success",
                                callback: function () {
                                    location.reload();
//                                    $("#login").val('');
//                                    $("#senha").val('');
//
//                                    $(".bootbox").modal('hide');
//                                    $(".modal-open").setAttribute("style", "overflow: auto");
                                    
                                    
//                                    setTimeout(function () {
//                                        $("#mytable").load("your-current-page.html #mytable");
//                                    }, 2000);
                                }
                            }
                        }
                    });
                } else if (data.flag == 'email') {
                    bootbox.dialog({
                        message: "Informe um email válido e tente novamente!",
                        title: "Mensagem",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success",
                                callback: function () {
                                }
                            }
                        }
                    });
                    } else if (data.flag == 'alterado') {
                        bootbox.dialog({
                            message: "E-mail já cadastrado no sistema. A senha original deve ser utilizada!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                        location.reload();
                                    }
                                }
                            }
                        });
                    } else if (data.flag == 'existe') {
                        bootbox.dialog({
                            message: "O email já está em uso, tente novamente!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    } else if (data.flag == 'participa') {
                        bootbox.dialog({
                            message: "O usuário já está incluso neste projeto, tente novamente!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    }
            },
            error: function () {
            }
        });
        return false;
    });
});