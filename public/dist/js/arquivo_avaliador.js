$(document).ready(function ()
{
    $('#projetoUpload').on("submit", function (e)
    {
        e.preventDefault();

        var form = $('#projetoUpload')[0];
        var data = new FormData(form);
        editarProjeto(data);
    });
});

function editarProjeto(data) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        url: baseUrl + 'projeto/editar',
        async: false,
        data: data,
        success: function (data) {
            if (data.flag === 'ok') {
                bootbox.dialog({
                    message: "Projeto atualizado com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        }
                    }
                });
            }

            if (data.flag === 'nok') {
                bootbox.dialog({
                    message: "Não foi possível atualizar o projeto!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        }
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function concluirProjeto(idprojeto) {
//    var idprojeto = idprojeto;

    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/editar-status',
        async: false,
        data: {
            idprojeto: idprojeto
        },
        beforeSend: function () {

        },
        success: function (data) {

            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Projeto Criado com Sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.href = baseUrl + 'admin-avaliador';
                            }
                        }
                    }
                });
            }
            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível efetuar a criação do projeto!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        }
                    }
                });
            }
        },
        complete: function () {
        },
        error: function () {
        }
    });
}

function adicionarCodificador(idcodificador, idtrecho, nome, arquivo, projeto) {
    var value = idcodificador + ',' + idtrecho + ',' + nome + ',' + arquivo + ',' + projeto + ',' + login;
    var values = idcodificador + '' + idtrecho;
//    var val = idcodificador + "," + idtrecho +",'"+nome+"'";

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        url: baseUrl + 'codificador-trecho/cadastrar/id/' + value,
        async: false,
        beforeSend: function () {

        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Codificador adicionado com Sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                $(".bootbox").modal('hide');
//                                $(".modal").setAttribute("style", "overflow: auto");
                                $(".tda" + values + "").remove();
                                // Get the child element node
                                var td = document.getElementsByClassName("tda" + values + "");
                                // Remove the child element from the document
                                
                                var html = "<tr class='tdb" + values + "'>";
                                html += "<td>" + nome + "</td>";
                                html += "<td><a class='btn btn-sm btn-danger' onclick='removerCodificador(" + idcodificador + "," + idtrecho + ',"' + nome + '"' + ")' title='Remover Codificador'><i class='glyphicon glyphicon-trash'></i></a></td>";
                                html += "</tr>";

                                $(".tbody_codificadores_adicionados" + idtrecho).append(html);
                                
                                td.remove();
                            }
                        }
                    }
                });
            }
            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível efetuar a importação do arquivo!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        }
                    }
                });
            }
        },
        complete: function () {
        },
        error: function () {
            var html = "<tr class='tdb" + values + "'>";
            html += "<td>" + nome + "</td>";
            html += "<td><a class='btn btn-sm btn-danger' onclick='removerCodificador(" + idcodificador + "," + idtrecho + ',"' + nome + '"' + ")' title='Remover Codificador'><i class='glyphicon glyphicon-trash'></i></a></td>";
            html += "</tr>";

            $(".tbody_codificadores_adicionados" + idtrecho).append(html);

            bootbox.dialog({
                message: "Codificador adicionado com Sucesso!",
                title: "Aviso",
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-success",
                        callback: function () {
                            $(".bootbox").modal('hide');
                            $(".tda" + values + "").remove();
                            var td = document.getElementsByClassName("td" + values + "");
                            td.remove();



                        }
                    }
                }
            });
        }
    });
}

function removerCodificador(idcodificador, idtrecho, nome) {
    var value = idcodificador + ',' + idtrecho;
    var values = idcodificador + '' + idtrecho;
//    var val = idcodificador + "," + idtrecho+","+nome+"'";

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        url: baseUrl + 'codificador-trecho/remover/id/' + value,
        async: false,
        beforeSend: function () {

        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Codificador Removido com Sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                var td = document.getElementsByClassName("tdb" + values + "");
                                td.remove();
//                                var html = "<tr class='tda'"+values+">";
//                                html += "<td>"+nome+"</td>";
//                                html += "<td><a class='btn btn-sm btn-primary' onclick='adicionarCodificador("+value+") title='Adicionar Codificador'>Adicionar</a></td>";
//                                html += "</tr>";
//                                
//                                alert(html);


                            }
                        }
                    }
                });
            }
            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível efetuar a importação de trecho!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
//                                location.reload();
                            }
                        }
                    }
                });
            }
        },
        complete: function () {
        },
        error: function () {
            var html = "<tr class='tda" + values + "'>";
            html += "<td>" + nome + "</td>";
            html += "<td><a class='btn btn-sm btn-primary' onclick='adicionarCodificador(" + idcodificador + "," + idtrecho + ',"' + nome + '"' + ")' title='Adicionar Codificador'>Adicionar ao arquivo</a></td>";
            html += "</tr>";

            $(".tbody_codificadores_geral" + idtrecho).append(html);

            bootbox.dialog({
                message: "Codificador Removido com Sucesso!",
                title: "Aviso",
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-success",
                        callback: function () {
                            $(".bootbox").modal('hide');
                            $(".tdb" + values + "").remove();
                            var td = document.getElementsByClassName("td" + values + "");
                            td.remove();
                        }
                    }
                }
            });
        }
    });
}

function remover(idtrecho) {

    bootbox.dialog({
        message: "Deseja realmente excluir este arquivo ?",
        title: "Aviso!",
        buttons: {
            success: {
                label: "Sim",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'trecho/remover',
                        async: false,
                        data: {idtrecho: idtrecho},
                        success: function (data) {
                            if (data.flag == 'ok') {
                                bootbox.dialog({
                                    message: "Arquivo excluído com sucesso!",
                                    title: "Aviso",
                                    buttons: {
                                        success: {
                                            label: "OK",
                                            className: "btn-success",
                                            callback: function () {
                                                location.reload();
                                            }
                                        }
                                    }
                                });
                            }
                        },
                        error: function (msg) {
                        }
                    });
                }
            },
            danger: {
                label: "Não",
                className: "btn btn-default",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}