$(document).ready(function () {
//    $('#codificadorNew').on("submit", function (e)
//    {
//        e.preventDefault();
//
//        var nome = $("#nome").val();
//        var login = $("#login").val();
//        var senha = $("#senha").val();
//        var idprojeto = $("#idprojeto").val();
//
////        alert("login = " + login + "senha = " + senha + " idgrupo = " + idgrupo);
//
//        $.ajax({
//            type: "POST",
//            url: baseUrl + 'usuario/cadastrar',
//            async: false,
//            data: {
//                nome: nome,
//                login: login,
//                senha: senha,
//                idprojeto: idprojeto
//            },
//            success: function (data) {
//                if (data.flag == 'ok') {
//                    bootbox.dialog({
//                        message: "Codificador cadastrado com sucesso!",
//                        title: "Mensagem",
//                        buttons: {
//                            success: {
//                                label: "OK",
//                                className: "btn-success",
//                                callback: function () {
//                                    location.reload();
//                                }
//                            }
//                        }
//                    });
//                } else if (data.flag == 'nok') {
//                    bootbox.dialog({
//                        message: "Selecione corretamente todos os campos!",
//                        title: "Mensagem",
//                        buttons: {
//                            success: {
//                                label: "OK",
//                                className: "btn-success",
//                                callback: function () {
//                                }
//                            }
//                        }
//                    });
//                }
//            },
//            complete: function () {
////            alert('complete');
//            },
//            error: function (msg) {
//                alert('error ' + msg);
//            }
//        });
//        return false;
//    });

    $('.cadastrar-grupoprojeto').click(function ()
    {
        var descricao = $("#descricao").val();
        var idprojeto = $("#projeto").val();

//        alert("login = " + login + "senha = " + senha + " idgrupo = " + idgrupo);

        $.ajax({
            type: "POST",
            url: baseUrl + 'grupo-projeto/cadastrar',
            async: false,
            data: {descricao: descricao, idprojeto: idprojeto
            },
            success: function (data) {
                if (data.flag == 'ok') {
                    bootbox.dialog({
                        message: "Grupo cadastrado com sucesso!",
                        title: "Mensagem",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success",
                                callback: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                } else {
                    bootbox.dialog({
                        message: "Selecione corretamente todos os campos!",
                        title: "Mensagem",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success",
                                callback: function () {
                                }
                            }
                        }
                    });
                }
            },
            complete: function () {
//            alert('complete');
            },
            error: function (msg) {
                alert('error ' + msg);
            }
        });
        return false;
    });

    $('.cadastrar-grupocodigo').click(function ()
    {
        var descricao = $("#descricao").val();
        var idprojeto = $("#projeto").val();

//        alert("login = " + login + "senha = " + senha + " idgrupo = " + idgrupo);

        $.ajax({
            type: "POST",
            url: baseUrl + 'grupo-codigo/cadastrar',
            async: false,
            data: {descricao: descricao, idprojeto: idprojeto
            },
            success: function (data) {
                if (data.flag == 'ok') {
                    bootbox.dialog({
                        message: "Grupo de Código cadastrado com sucesso!",
                        title: "Mensagem",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success",
                                callback: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                } else {
                    bootbox.dialog({
                        message: "Selecione corretamente todos os campos!",
                        title: "Mensagem",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success",
                                callback: function () {
                                }
                            }
                        }
                    });
                }
            },
            complete: function () {
//            alert('complete');
            },
            error: function (msg) {
                alert('error ' + msg);
            }
        });
        return false;
    });

    $('.finalizar-codificacao').click(function ()
    {
//    alert("finalizando codificação");
        var idusuario;
        var idcodigo;
        var idgrupo;
        var idtrecho = $("#idtrecho").val();
        var status;
        var usuario = [];
        var codigo = [];
        var grupo = [];
        var trecho = [];
        var statusCodigo = [];
        var cont = 0;

////        Coletando todos os codigos 
        $('.ids').each(function () {
            usuario.push($(this).attr("itemid"));
            codigo.push($(this).attr("itemtype"));
            grupo.push($(this).attr("itemprop"));
            trecho.push($(this).attr("itemscope"));
            statusCodigo.push($(this).attr("value"));
            cont++;
        });

        idusuario = usuario.toString();
        idcodigo = codigo.toString();
        idgrupo = grupo.toString();
        status = statusCodigo.toString();

//    alert("idusuario = " + idusuario + "idgrupo = " + idgrupo + " idtrecho = " + idtrecho);
//    alert("idcodigo = " + idcodigo + " status = " + status);

        $.ajax({
            type: "POST",
            url: baseUrl + 'avaliacao/editar-status',
            async: false,
            data: {idusuario: idusuario, status: status, idcodigo: idcodigo,
                idgrupo: idgrupo, idtrecho: idtrecho
            },
            success: function () {
                bootbox.dialog({
                    message: "Codificação finalizada com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            },
            complete: function () {
//            alert('complete');
            },
            error: function (msg) {
                alert('error ' + msg);
            }
        });
        return false;
    });

    $('#codigoNew').on("submit", function (e)
    {
        e.preventDefault();

        var form = $('#codigoNew')[0];
        var data = new FormData(form);

        cadastrarCodigo(data);
    });

    $('#grupoNew').on("submit", function (e)
    {
        e.preventDefault();

        var form = $('#grupoNew')[0];
        var data = new FormData(form);

        cadastrarGrupo(data);
    });

});

function agruparCodigo(id) {
    var idgrupocodigo = $("#idgrupocodigo" + id).val();

    $.ajax({
        type: "POST",
        url: baseUrl + 'admin/agrupar-codigo',
        async: false,
        data: {id: id,
            idgrupocodigo: idgrupocodigo
        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Categorização realizada com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function cadastrarCodigo(data) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        url: baseUrl + 'admin/cadastrar-codigo-aceito',
        async: false,
        data: data,
        success: function (data) {
            if (data.flag === 'ok') {
                bootbox.dialog({
                    message: "Código cadastrado com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function cadastrarGrupo(data) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        url: baseUrl + 'grupo-codigo/cadastrar',
        async: false,
        data: data,
        success: function (data) {
            if (data.flag === 'ok') {
                bootbox.dialog({
                    message: "Categoria cadastrada com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function editarCodigo(id) {
    var descricao = $("#descricao" + id).val();

    $.ajax({
        type: "POST",
        url: baseUrl + 'admin/editar-codigo',
        async: false,
        data: {id: id,
            descricao: descricao
        },
        success: function (data) {
            if (data.flag === 'ok') {
                bootbox.dialog({
                    message: "Código atualizado com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function aceitarCodigo(id) {
//    alert(id + ' ' + status);
    $.ajax({
        type: "POST",
        url: baseUrl + 'admin/aceito',
        async: false,
        data: {id: id
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert("error");
        }
    });
}

function listarTrechosPorGrupo(id) {

    $.ajax({
        type: "POST",
        url: baseUrl + 'admin/listar',
        async: false,
        data: {id: id
        },
        success: function () {
//            location.href = baseUrl + 'admin/listar/id/'+id;
//            location.href = "#"+id;
        },
        error: function () {
//                alert("error");
        }
    });
}

function excluirCodigo(id) {
    bootbox.dialog({
        message: "Deseja realmente excluir este Código ?",
        title: "Aviso!",
        buttons: {
            success: {
                label: "Confirmar",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'admin/excluir-codigo/id/' + id,
                        async: false,
                        beforeSend: function () {
//                            alert(data);
                        },
                        success: function () {
//                            alert(data);
                            location.reload();
                        },
                        complete: function () {
//                            location.reload();
                        },
                        error: function () {
//                            alert(msg);

                        }
                    });
                }
            },
            cancel: {
                label: "Cancelar",
                className: "btn-primary",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}

function excluirCodigoAvaliador(id) {
    bootbox.dialog({
        message: "Deseja realmente excluir este Código ?",
        title: "Aviso!",
        buttons: {
            success: {
                label: "Confirmar",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'admin/excluir-codigo-avaliador/id/' + id,
                        async: false,
                        beforeSend: function () {
//                            alert(data);
                        },
                        success: function () {
//                            alert(data);
                            location.reload();
                        },
                        complete: function () {
//                            location.reload();
                        },
                        error: function () {
//                            alert(msg);

                        }
                    });
                }
            },
            cancel: {
                label: "Cancelar",
                className: "btn-primary",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}

function salvarCodigo(idtrecho) {

    var idgrupocodigo = $("#idgrupocodigo").val();
    var descricao = $("#descricao").val();

    $.ajax({
        type: "POST",
        url: baseUrl + 'codigo/cadastrar-codigo-aceito',
        async: false,
        data: {idtrecho: idtrecho, idgrupocodigo: idgrupocodigo, descricao: descricao
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert("error");
        }
    });
}

function editarGrupo(id)
{
//    alert(id);
    var descricao = $("#grupoeditar" + id).val();
//    alert(descricao);
//    alert(descricao);

    $.ajax({
        type: "POST",
        url: baseUrl + 'grupo-codigo/editar',
        async: false,
        data: {id: id, descricao: descricao
        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Categoria atualizada com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function editarUsuario(id)
{
    var login = $("#login" + id).val();
//    alert(descricao);

    $.ajax({
        type: "POST",
        url: baseUrl + 'usuario/editar',
        async: false,
        data: {id: id, login: login
        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Codificador atualizado com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            } else {
                bootbox.dialog({
                    message: "Selecione corretamente todos os campos!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function excluirUsuario(id) {

    bootbox.dialog({
        message: "Deseja realmente excluir?",
        title: "Mensagem de Alerta!",
        buttons: {
            success: {
                label: "Sim",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'usuario/excluir/id/' + id,
                        success: function (data) {
                            location.reload();
                        },
                        complete: function () {
                            location.reload();
                        },
                        error: function (msg) {
                            alert(msg);

                        }
                    });
                }
            },
            danger: {
                label: "Não",
                className: "btn-default",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}

function excluirCategoria(id) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'admin/excluir-categoria',
        async: false,
        data: {id: id
        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Categoria excluída com sucesso!",
                    title: "Mensagem",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        }
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

//function agruparCodigo(id)
//{
////    alert(id);
//    var descricao = $("#grupoeditar" + id).val();
////    alert(descricao);
////    alert(descricao);
//
//    $.ajax({
//        type: "POST",
//        url: baseUrl + 'grupo-codigo/editar',
//        async: false,
//        data: {id: id, descricao: descricao
//        },
//        success: function (data) {
//            if (data.flag == 'ok') {
//                bootbox.dialog({
//                    message: "Grupo atualizado com sucesso!",
//                    title: "Mensagem",
//                    buttons: {
//                        success: {
//                            label: "OK",
//                            className: "btn-success",
//                            callback: function () {
//                                location.reload();
//                            }
//                        },
//                    }
//                });
//            }
//        },
//        error: function () {
//            alert("error");
//        }
//    });
//}