$(document).ready(function () {

    $('#criarContaNew').on("submit", function (e)
    {
        e.preventDefault();

        var login = $("#login").val();
        var senha = $("#senha").val();
        var confirmar = $("#confirmar-senha").val();

        if (senha == confirmar) {
            $.ajax({
                type: "POST",
                url: baseUrl + 'criar-conta/cadastrar',
                async: false,
                data: {
                    login: login, senha: senha
                },
                success: function (data) {
                    if (data.flag == 'ok') {
                        bootbox.dialog({
                            message: "Avaliador cadastrado com sucesso!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                        window.location.href = "http://opencodingtool.com.br/public";
                                    }
                                }
                            }
                        });
                    } else if (data.flag == 'nok') {
                        bootbox.dialog({
                            message: "Preencha corretamente e tente novamente!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    } else if (data.flag == 'avaliador') {
                        bootbox.dialog({
                            message: "Já existe avaliador utilizando este email, tente novamente!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    } else if (data.flag == 'alterado') {
                        bootbox.dialog({
                            message: "O email esta vinculado para avaliador e codificador, utilize a mesma senha inicial!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                        window.location.href = "http://opencodingtool.com.br/public";
                                    }
                                }
                            }
                        });
                    } else if (data.flag == 'existe') {
                        bootbox.dialog({
                            message: "O email já está em uso, tente novamente!",
                            title: "Alerta",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "btn-success",
                                    callback: function () {
                                    }
                                }
                            }
                        });
                    } else if (data.flag == 'email') {
                    bootbox.dialog({
                        message: "Informe um email válido e tente novamente!",
                        title: "Mensagem",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success",
                                callback: function () {
                                }
                            }
                        }
                    });
                }
                },
                complete: function () {
//            alert('complete');
                },
                error: function (msg) {
                    alert('error ' + msg);
                }
            });
        } else {
            bootbox.dialog({
                message: "Repita o campo senha novamente!",
                title: "Mensagem",
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-success",
                        callback: function () {
                        }
                    }
                }
            });
        }
        return false;
    });
});