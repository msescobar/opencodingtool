$(document).ready(function ()
{
    $('#redefinir-senha').on("submit", function (e)
    {
        e.preventDefault();

//        $('.loading').removeClass("hidden");
//        $('.btn-success').addClass("hidden");

        var form = $('#redefinir-senha')[0];
        var data = new FormData(form);

        redefinirSenha(data);
    });
});

function redefinirSenha(data) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
        url: baseUrl + 'usuario/alterar-senha',
        data: data,
        async: false,
        beforeSend: function () {
//            $('.loading').addClass("hidden");
//            $('.btn-success').removeClass("hidden");
        },
        success: function (data) {
            if (data.flag === 'ok') {
                bootbox.dialog({
                    message: "Redefinição de senha realizada com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            } else if (data.flag === 'nok') {
                bootbox.dialog({
                    message: "Os campos devem estar iguais!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                            }
                        },
                    }
                });
            }
        },
        complete: function () {
//            alert('complete');
        },
        error: function (msg) {
//                alert('Entre em contato com a manutenção! Erro: ' + msg);
        }
    });
}

function solicitarSenha() {
    var email = $('.email').val();

    $.ajax({
        type: "POST",
        url: baseUrl + 'usuario/nova-senha',
        dataType: 'json',
        data: {
            email: email
        },
        async: false,
        beforeSend: function () {
        },
        success: function (data) {

            if (data.flag === 'ok') {
                bootbox.dialog({
                    message: "Verifique o seu email para redefinir a sua senha!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            } else if (data.flag === 'nok') {
                bootbox.dialog({
                    message: "Nenhum registro foi encontrado, tente novamente!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
//                                location.reload();
                            }
                        },
                    }
                });
            } 
        },
        complete: function () {
        },
        error: function () {
        }
    });
}