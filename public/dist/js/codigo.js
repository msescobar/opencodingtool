$(document).ready(function ()
{
    $('#codigoNew').on("submit", function (e)
    {
        e.preventDefault();
        var form = $('#codigoNew')[0];
        var data = new FormData(form);
        salvarCodigo(data);
    });

    $('.exibir-codigos').click(function () {
        $(".codigos").removeClass("fade");
    });

});

function salvarCodigo(data) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
//        url: baseUrl + 'codigo/cadastrar',
        url: baseUrl + 'admin-codificador/cadastrar',
        async: false,
        data: data,
        success: function () {
            bootbox.dialog({
                message: "Código cadastrado com sucesso!",
                title: "Mensagem",
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-success",
                        callback: function () {
                            location.reload();
                        }
                    },
                }
            });
        },
        error: function () {
        }
    });
}

function finalizarCodificacao(idavaliacao) {
//    alert("enviar-avaliacao");
    var idcodigo;
    var status;
    var codigo = [];
    var statusCodigo = [];
    var cont = 0;

//        Coletando todos os codigos 
    $('.idds').each(function () {

        codigo.push($(this).attr("itemtype"));
        statusCodigo.push($(this).attr("value"));
        cont++;
    });

    idcodigo = codigo.toString();
    status = statusCodigo.toString();

//    alert("idcodigo = " + idcodigo + " status = " + status);

    $.ajax({
        type: "POST",
        url: baseUrl + 'codigo/editar-status',
        async: false,
        data: {status: status, idcodigo: idcodigo, idavaliacao: idavaliacao,
        },
        success: function () {
            bootbox.dialog({
                message: "Código enviado para avaliação, com sucesso!",
                title: "Mensagem",
                buttons: {
                    success: {
                        label: "OK",
                        className: "btn-success",
                        callback: function () {
                            location.reload();
                        }
                    },
                }
            });
        },
        complete: function () {
//            alert('complete');
        },
        error: function () {
            alert('error');
        }
    });
//        return false;
}

function excluirCodigo(id) {
    bootbox.dialog({
        message: "Deseja realmente excluir este código ?",
        title: "Aviso!",
        buttons: {
            success: {
                label: "Sim",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'admin-codificador/excluir-codigo/id/' + id,
                        success: function (data) {
                            if (data.flag == 'ok') {
                                bootbox.dialog({
                                    message: 'Codigo Excluído com sucesso!',
                                    title: 'Aviso',
                                    buttons: {
                                        success: {
                                            label: "OK",
                                            className: "btn-success",
                                            callback: function () {
                                                location.reload();
                                            }
                                        }
                                    }
                                });
                            }
                            if (data.flag == 'nok') {
                                bootbox.alert('Não é possível excluir este cadastro, pois ainda está em uso!');
                            }
                        },
                        error: function () {
                        }
                    });
                }
            },
            danger: {
                label: "Não",
                className: "btn btn-default",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}

function excluirCodigoUsuario(id, idtrecho) {

    bootbox.dialog({
        message: "Deseja realmente excluir ?",
        title: "Mensagem de Alerta!",
        buttons: {
            success: {
                label: "Sim",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'codigo/excluir-codigo/id/' + id,
                        success: function (data) {
//                            if (data == 'ok') {
//                                bootbox.dialog({
//                                    message: 'Codigo Excluído com sucesso!',
//                                    title: 'Caixa de diálogo',
//                                    buttons: {
//                                        success: {
//                                            label: "OK",
//                                            className: "btn-success",
//                                            callback: function () {
                            location.href = baseUrl + 'codigo/listar/id/' + idtrecho;
//                                            }
//                                        }
//                                    }
//                                });
//                            }
//                            if (data == 'nok') {
//                                bootbox.alert('Não é possível excluir este cadastro, pois ainda está em uso!');
//                            }
                        },
                        complete: function () {
                            location.href = baseUrl + 'codigo/listar/id/' + idtrecho;
                        },
                        error: function (msg) {
//                            alert(msg);
                            location.href = baseUrl + 'codigo/listar/id/' + idtrecho;
                        }
                    });
                }
            },
            danger: {
                label: "Não",
                className: "btn btn-default",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}

//function salvarCodigo() {
//
//    var idtrecho = $("#idtrecho").val();
//    var codigo = $("#codigo").val();
//    var idgrupocodigo = $("#idgrupocodigo").val();

//    $.ajax({
//        type: "POST",
//        url: baseUrl + 'codigo/cadastrar',
//        async: false,
//        data: {idtrecho: idtrecho, codigo: codigo,
//            idgrupocodigo: idgrupocodigo
//        },
//        success: function () {
//            location.reload();
//        },
//        error: function () {
//        }
//    });
//}

function editarCodigo(id) {
//    alert(id);
    var descricao = $("#descricao" + id).val();
//    var idgrupocodigo = $("#idgrupocodigo" + id).val();

    $.ajax({
        type: "POST",
        url: baseUrl + 'admin/editar-codigo',
        async: false,
        data: {id: id, descricao: descricao
        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Código atualizado com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
//                                location.href = baseUrl + 'codigo/listar/id/' + idtrecho;
                            }
                        },
                    }
                });
            }

            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível atualizar o código!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.href = baseUrl + 'codigo/listar/id/' + idtrecho;
                            }
                        },
                    }
                });
            }
        },
        error: function () {
            alert("error");
        }
    });
}

function finalizar(idtrecho) {

    bootbox.dialog({
        message: "Deseja realmente finalizar está codificação ?",
        title: "Mensagem de Alerta!",
        buttons: {
            success: {
                label: "Sim",
                className: "btn-success",
                callback: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + 'admin-codificador/finalizar-codificacao',
                        async: false,
                        data: {idtrecho: idtrecho},
                        success: function (data) {
                            if (data.flag == 'ok') {
                                bootbox.dialog({
                                    message: "Codificação finalizada com sucesso!",
                                    title: "Aviso",
                                    buttons: {
                                        success: {
                                            label: "OK",
                                            className: "btn-success",
                                            callback: function () {
                                                location.reload();
                                            }
                                        }
                                    }
                                });
                            }
                        },
                        error: function (msg) {
                        }
                    });
                }
            },
            danger: {
                label: "Não",
                className: "btn btn-default",
                callback: function () {
                    setTimeout(function () {
                        // that's enough of that
                        bootbox.hideAll();
                    }, 3000);
                }
            }
        }
    });
}