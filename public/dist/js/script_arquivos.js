$(document).ready(function ()
{
    $('#importar-arquivo').on("submit", function (e)
    {
        e.preventDefault();

        var form = $('#importar-arquivo')[0];
        var data = new FormData(form);
        cadastrarArquivo(data);
    });

    $("#arquivo").change(function () {
        var allowedTypes = ['application/pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'];
        var file = this.files[0];
        var fileType = file.type;
        if (!allowedTypes.includes(fileType)) {
            bootbox.alert({
                title: "Mensagem de Aviso!",
                message: "Por favor selecione um arquivo válido!" + "<br>" + "PDF ou DOCX" + "<br>" + "Tente novamente! ",
            });
            $("#arquivo").val('');
            return false;
        }
    });
});


function cadastrarArquivo(data) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false, // Important!
        contentType: false,
        cache: false,
//        url: baseUrl + 'trecho/teste',
        url: baseUrl + 'trecho/importar-arquivo',
        async: false,
        data: data,
        beforeSend: function () {

        },
        success: function (data) {
            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Importação de Arquivo realizado com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível efetuar a importação de trecho!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.reload();
                            }
                        },
                    }
                });
            }
        },
        complete: function () {
        },
        error: function () {
        }
    });
}
