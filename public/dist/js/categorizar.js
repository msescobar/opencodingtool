var example1 = document.getElementById('example1'),
        example2Left = document.getElementById('example2-left'),
        example2Right = document.getElementById('example2-right'),
        example3Left = document.getElementById('example3-left'),
        example3Right = document.getElementById('example3-right'),
        example4Left = document.getElementById('example4-left'),
        example4Right = document.getElementById('example4-right'),
        example5 = document.getElementById('example5'),
        example6 = document.getElementById('example6'),
        example7 = document.getElementById('example7'),
        gridDemo = document.getElementById('gridDemo'),
        multiDragDemo = document.getElementById('multiDragDemo'),
        swapDemo = document.getElementById('swapDemo');

new Sortable(example2Left, {
    group: 'shared', // set both lists to same group
    animation: 150
});

new Sortable(example2Right, {
    group: 'shared',
    animation: 150,
    onStart: function (evt) {

    },
    onEnd: function (evt) {
        alert(evt.item.id);
        alert(document.getElementById(evt.to.id).getAttribute('itemid'));
    }
});

new Sortable(example3Right, {
    group: 'shared',
    animation: 150,
    onStart: function () {

    },
    onEnd: function () {
    }
});