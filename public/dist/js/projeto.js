$('#projetoNew').on("submit", function (e)
{
    e.preventDefault();

    var titulo = $('#titulo').val();
    var descricao = $('#descricao').val();

    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/cadastrar',
        async: false,
        data: {titulo: titulo, descricao: descricao},
        success: function (data) {
            
            location.href = baseUrl + 'arquivo/index/id/'+data.idprojeto;

//            if (data.flag == 'ok') {
//                bootbox.dialog({
//                    message: "Projeto cadastrado com sucesso!",
//                    title: "Aviso",
//                    buttons: {
//                        success: {
//                            label: "OK",
//                            className: "btn-success",
//                            callback: function () {
//                                location.href = baseUrl + 'arquivo/index/id/'+data.idprojeto;
//                            }
//                        }
//                    }
//                });
//            }

        },
        complete: function () {
        },
        error: function (error) {
            console.log(error);
        }
    });
//        return false;
});

function editarProjeto(idprojeto) {

    var titulo = $('#titulo' + idprojeto).val();
    var descricao = $('#descricao' + idprojeto).val();

    $.ajax({
        type: "POST",
        url: baseUrl + 'projeto/editar',
        async: false,
        data: {idprojeto: idprojeto, titulo: titulo, descricao: descricao},
        success: function (data) {

            if (data.flag == 'ok') {
                bootbox.dialog({
                    message: "Projeto atualizado com sucesso!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

            if (data.flag == 'nok') {
                bootbox.dialog({
                    message: "Não foi possível atualizar o projeto!",
                    title: "Aviso",
                    buttons: {
                        success: {
                            label: "OK",
                            className: "btn-success",
                            callback: function () {
                                location.href = baseUrl + 'projeto/index';
                            }
                        },
                    }
                });
            }

        },
        complete: function () {
        },
        error: function (error) {
            console.log(error);
        }
    });
//        return false;
}


$(function () {
    $("#data_inicial").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true
    });
    $("#data_final").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true
    });
});