<?php

class Application_Model_Usuario extends Zend_Db_Table_Row_Abstract {

//    private $idusuario;
//    private $login;
//    private $senha;
//    private $email;
//    private $idperfil;
//    private $redefinir;
//    private $status;
    
    function getIdusuario()
    {
        return $this->idusuario;
    }

    function getLogin()
    {
        return $this->login;
    }

    function getSenha()
    {
        return $this->senha;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getIdperfil()
    {
        return $this->idperfil;
    }

    function getRedefinir()
    {
        return $this->redefinir;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;
    }

    function setLogin($login)
    {
        $this->login = $login;
    }

    function setSenha($senha)
    {
        $this->senha = $senha;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function setIdperfil($idperfil)
    {
        $this->idperfil = $idperfil;
    }

    function setRedefinir($redefinir)
    {
        $this->redefinir = $redefinir;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }
    
}