<?php

class Application_Model_CodificadorTrecho extends Zend_Db_Table_Row_Abstract
{
//    private $idcodificadortrecho;
//    private $idcodificador;
//    private $idtrecho;
//    private $status;
    
    function getIdcodificadortrecho()
    {
        return $this->idcodificadortrecho;
    }

    function getIdcodificador()
    {
        return $this->idcodificador;
    }

    function getIdtrecho()
    {
        return $this->idtrecho;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setIdcodificadortrecho($idcodificadortrecho)
    {
        $this->idcodificadortrecho = $idcodificadortrecho;
    }

    function setIdcodificador($idcodificador)
    {
        $this->idcodificador = $idcodificador;
    }

    function setIdtrecho($idtrecho)
    {
        $this->idtrecho = $idtrecho;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }


    
    
}