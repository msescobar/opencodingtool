<?php

class Application_Model_Codigo extends Zend_Db_Table_Row_Abstract
{
//    private $idcodigo;
//    private $idtrecho;
//    private $idusuario;
//    private $status;
//    private $descricao;
    
    function getIdcodigo()
    {
        return $this->idcodigo;
    }

    function getIdtrecho()
    {
        return $this->idtrecho;
    }

    function getIdusuario()
    {
        return $this->idusuario;
    }

    function getStatus()
    {
        return $this->status;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function setIdcodigo($idcodigo)
    {
        $this->idcodigo = $idcodigo;
    }

    function setIdtrecho($idtrecho)
    {
        $this->idtrecho = $idtrecho;
    }

    function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }
    
}