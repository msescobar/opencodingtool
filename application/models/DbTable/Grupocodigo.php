<?php

class Application_Model_DbTable_Grupocodigo extends Zend_Db_Table_Abstract {

    protected $_name = 'grupocodigo';
    protected $_rowClass = "Application_Model_Grupocodigo";

    public function cadastrar($dados)
    {
//        var_dump($dados);die();
        $grupocodigo = $this->createRow();
        /* @var $grupocodigo Application_Model_Grupocodigo */
        $grupocodigo->setIdprojeto(intval($dados['idprojeto']));
        $grupocodigo->setDescricao($dados['grupo']);
        $grupocodigo->setStatus(0);

        return $grupocodigo->save();
    }

    public function editar($id, $dados)
    {
//        var_dump($dados['descricao']); die();
        $grupocodigo = $this->find(intval($id))->current();
//        var_dump($grupocodigo); die();
        /* @var $grupocodigo Application_Model_Grupocodigo */
//        $grupocodigo->setIdtrecho($dados['idtrecho']);
        $grupocodigo->setDescricao($dados['descricao']);

        return $grupocodigo->save();
    }

    public function excluir($id)
    {
        $select = $this->select()->where('idgrupocodigo = ?', $id);
        $grupocodigo = $this->fetchRow($select);

        return $grupocodigo->delete();
    }

    public static function getGrupoCodigosPorIdAvaliador($idavaliador, $idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT grupocodigo.*, trecho.descricao as trecho,"
                . " projeto.descricao as projeto"
                . " FROM grupocodigo, projeto, trecho, avaliador, usuario WHERE"
                . " usuario.idusuario = avaliador.idusuario AND"
                . " grupocodigo.idtrecho = trecho.idtrecho AND"
                . " trecho.idprojeto = projeto.idprojeto AND"
                . " projeto.idavaliador = avaliador.idavaliador AND"
                . " grupocodigo.idtrecho = '$idtrecho' AND"
                . " projeto.idavaliador = '$idavaliador'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getGrupoCodigosPorIdAvaliadorPorProjeto($idavaliador, $idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT grupocodigo.*,"
                . " projeto.descricao as projeto"
                . " FROM grupocodigo, projeto, avaliador, usuario WHERE"
                . " usuario.idusuario = avaliador.idusuario AND"
                . " grupocodigo.idprojeto = projeto.idprojeto AND"
                . " projeto.idavaliador = avaliador.idavaliador AND"
                . " projeto.idprojeto = '$idprojeto' AND"
                . " projeto.idavaliador = '$idavaliador'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getGrupoCodigosPorIdTrecho($idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT grupocodigo.* FROM grupocodigo, trecho WHERE"
                . " projeto.idtrecho = grupocodigo.idtrecho AND"
                . " projeto.idtrecho = '$idtrecho'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getGrupoCodigoPorIdTrecho($idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT grupocodigo.* FROM grupoprojeto, grupocodigo, projeto, trecho WHERE"
                . " trecho.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
                . " grupoprojeto.idprojeto = projeto.idprojeto AND"
                . " grupocodigo.idprojeto = projeto.idprojeto AND"
                . " trecho.idtrecho = '$idtrecho'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

}
