<?php

class Application_Model_DbTable_Grupocodificadorprojeto extends Zend_Db_Table_Abstract {

    protected $_name = 'grupocodificadorprojeto';
    protected $_rowClass = "Application_Model_Grupocodificadorprojeto";

    public function cadastrar($idgrupoprojeto, $idcodificador)
    {
        $grupocodificadorprojeto = $this->createRow();
        /* @var $grupocodificadorprojeto Application_Model_Grupocodificadorprojeto */
        $grupocodificadorprojeto->setIdgrupoprojeto($idgrupoprojeto);
        $grupocodificadorprojeto->setIdcodificador($idcodificador);

        return $grupocodificadorprojeto->save();
    }
    
    public function getteste()
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT grupocodificadorprojeto.* FROM grupocodificadorprojeto");
        
        $rows = $stmt->fetchAll();
        
        return $rows;
    }
   
    public function getTodosProjetosPorIdCodificador($idcodificador)
    {
//        var_dump($idcodificador);die();
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT projeto.* FROM projeto, codificador, grupocodificadorprojeto WHERE"
                . " grupocodificadorprojeto.idcodificador = codificador.idcodificador AND"
                . " grupocodificadorprojeto.idprojeto = projeto.idprojeto AND"
                . " projeto.status = 1 AND"
                . " grupocodificadorprojeto.idcodificador = '" . $idcodificador . "'");
        
        $rows = $stmt->fetchAll();
        
        return $rows;
    }

}
