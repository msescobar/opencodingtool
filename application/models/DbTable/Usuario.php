<?php

class Application_Model_DbTable_Usuario extends Zend_Db_Table_Abstract {

    protected $_name = 'usuario';
    protected $_rowClass = "Application_Model_Usuario";

    public static function getAdapterPersonalizado()
    {
        $adapter = new Zend_Db_Adapter_Pdo_Mysql(array(
            'driver' => 'pdo_mysql',
            'dbname' => 'bdtcc',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8'
        ));

//        $adapter = new Zend_Db_Adapter_Pdo_Mysql(array(
//            'driver' => 'pdo_mysql',
//            'dbname' => 'opencodingtool',
//            'username' => 'opencodingtool',
//            'password' => 'B612l214!',
//            'host' => 'opencodingtool.mysql.dbaas.com.br',
//            'charset' => 'utf8'
//        ));


        return $adapter;
    }

    public function cadastrar($dados)
    {
        $usuario = $this->createRow();
        /* @var $usuario Application_Model_Usuario */
        $usuario->setLogin($dados['login']);
        $usuario->setSenha(base64_encode($dados['senha']));
        $usuario->setIdperfil(3);
        $usuario->setStatus(0);
        $usuario->setRedefinir(0);

        return $usuario->save();
    }

    public function cadastrarAvaliador($dados)
    {
        $usuario = $this->createRow();
        /* @var $usuario Application_Model_Usuario */
        $usuario->setLogin($dados['login']);
        $usuario->setSenha(base64_encode($dados['senha']));
        $usuario->setIdperfil(2);
        $usuario->setStatus(0);
        $usuario->setRedefinir(0);

        return $usuario->save();
    }

    public function editar($id, $dados)
    {
        $usuario = $this->find($id)->current();
        /* @var $usuario Application_Model_Usuario */
        $usuario->setLogin($dados['login']);

        return $usuario->save();
    }

    public function editarAvaliador($id, $dados)
    {
        $usuario = $this->find($id)->current();
        /* @var $usuario Application_Model_Usuario */
        $usuario->setLogin($dados['login']);

        return $usuario->save();
    }
    
        public function editarPerfilParaAmbos($id)
    {
        $usuario = $this->find($id)->current();
        /* @var $usuario Application_Model_Usuario */
        $usuario->setIdperfil(4);

        return $usuario->save();
    }

    public function getAvaliadores($idperfil)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT usuario.* FROM usuario"
                . " WHERE usuario.status = 0 AND"
                . " usuario.idperfil = '$idperfil'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getUsuarioEsquecidoPorEmail($email)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM usuario WHERE"
                . " usuario.login = '$email'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

//    public function getUsuarioPorEmail($email)
//    {
//        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();
//
//        $stmt = $adapter->query("SELECT * FROM usuario, avaliador WHERE"
//                . " usuario.idusuario = avaliador.idusuario AND"
//                . " usuario.email = '$email'");
//
//        $rows = $stmt->fetchAll();
//
//        return $rows;
//    }

    public function getUsuarioPorLoginEmUso($login)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM usuario, avaliador WHERE"
                . " usuario.idusuario = avaliador.idusuario AND"
                . " usuario.login = '$login'");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
        public function getCodificadorPorLoginEmUso($login)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM usuario, codificador WHERE"
                . " usuario.idusuario = codificador.idusuario AND"
                . " usuario.login = '$login'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getUsuarioPorLoginComAvaliador($login, $idavaliador)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM usuario, codificador WHERE"
                . " usuario.idusuario = codificador.idusuario AND"
                . " usuario.login = '$login' AND"
                . " codificador.idavaliador = '$idavaliador'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getUsuarioPorEmailComAvaliador($email, $idavaliador)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM usuario, codificador WHERE"
                . " usuario.idusuario = codificador.idusuario AND"
                . " usuario.email = '$email' AND"
                . " codificador.idavaliador = '$idavaliador'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function excluir($id)
    {
        $this->find($id)->current();
        $usuario = $this->getUsuarioPorId($id);
        /* @var $usuario Application_Model_Usuario */

        $usuario->setStatus(1);


        return $usuario->save();
    }

    public function listarTodosUsuarios()
    {
        return $this->fetchAll();
    }

    public function getUsuarioPorLogin($login)
    {
        $select = $this->select()->where('login = ?', $login);

        return $this->fetchRow($select);
    }

    public function getUsuarioPorId($id)
    {
        $select = $this->select()->where('idusuario = ?', $id);

        return $this->fetchRow($select);
    }

    public function getStatusRedefinirPorId($id)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT usuario.redefinir FROM usuario WHERE"
                . " usuario.idusuario = '" . $id . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function setRedefinirSenha($id)
    {
        $usuario = $this->getUsuarioPorId($id);
        /* @var $usuario Application_Model_Usuario */
        $usuario->setRedefinir(1);

        return $usuario->save();
    }

    public function setSenhaAlterada($id)
    {
        $usuario = $this->getUsuarioPorId($id);
        /* @var $usuario Application_Model_Usuario */
        $usuario->setRedefinir(0);

        return $usuario->save();
    }

    public function renovar($dados, $idusuario)
    {
        $usuario = $this->getUsuarioPorId($idusuario);
        /* @var $usuario Application_Model_Usuario */

//            $usuario->setNome($dados['nome']);
//            $usuario->setLogin($dados['login']);

        if ($dados['senha'] !== '') {
            $usuario->setSenha(md5($dados['senha']));
        }

        return $usuario->save();
    }

    //redefinição de senha
    public function renovarSenha($email, $id)
    {
        $config = array('ssl' => 'tls',
            'auth' => 'login',
            'username' => 'mauriciosescobar@gmail.com',
//            'username' => 'b2b@caal.com.br',
            'port' => '587',
            'password' => 'hdtoknswuqdovoaf');
//            'password' => 'rxV$3Z2x');

//        $transport = new Zend_Mail_Transport_Smtp('mail.caal.com.br', $config);
        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

        $mail = new Zend_Mail('utf-8');
        $mailer = new Zend_Mail('utf-8');
//                    $mail->setBodyHtml($this->view->partial('templates/mail.phtml', $dados));
        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//PT" "http://www.w3.org/TR/html4/strict.dtd">
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br">
    <head>
        <title>CAAL - </title>
        <meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/0e31f58d04.js" crossorigin="anonymous"></script>
        <style>
            .section{
                border: 1px #006746 solid;
                border-radius: 15px;
            }

            .container{
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            div{
                display: block;
            }

            .header{
                background-color: white;
            }

            .verde-caal{
                color: white;
                background-color: #2fa4e7;
                border-radius: 5px;
            }

            .verde{
                color: green;
            }

            h2{
                text-align: center;
                font-family: sans-serif;
                font-size: 30px;
            }

            .text-center{
                text-align: center;
                font-family: sans-serif;
                font-size: 14px;
            }

            p{
                text-align: justify;
                font-family: sans-serif;
            }

            .tron{
                /*padding: 1rem 2rem;*/
                padding: 1rem;
            }

            .tron1{
                padding: 4rem 2rem;
            }

            .tron2{
                padding: 1rem 2rem;
            }

            .tron2.h2{
                padding: 1rem 2rem;
            }

            .tron3{
                padding: 1rem 1rem;
            }

            .linha{
                margin-right: -15px;
                margin-left: -15px;
            }

            .bloco{
                margin-left: auto;
                margin-right: auto;
                text-align: center;
            }

            .table {
              width: 100%;
              margin-bottom: 1rem;
              color: #212529;
            }

            .table th,
            .table td {
              padding: 0.75rem;
              vertical-align: top;
              border-top: 1px solid #dee2e6;
            }

            .table thead th {
              vertical-align: bottom;
              border-bottom: 2px solid #dee2e6;
            }

            .table tbody + tbody {
              border-top: 2px solid #dee2e6;
            }

            .table-sm th,
            .table-sm td {
              padding: 0.3rem;
            }

            .table-bordered {
              border: 1px solid #dee2e6;
            }

            .table-bordered th,
            .table-bordered td {
              border: 1px solid #dee2e6;
            }

            .table-bordered thead th,
            .table-bordered thead td {
              border-bottom-width: 2px;
            }

            .table-borderless th,
            .table-borderless td,
            .table-borderless thead th,
            .table-borderless tbody + tbody {
              border: 0;
            }
            
            .fa-instagram{
                color: #f58529;
            }
            
            .fa-whatsapp{
                color: #075E54;
            }
            
            .fa-facebook{
                color: #3B5998;
            }

            /*
            @media (min-width: 1200px){
                .container{
                    max-width: 1140px;
                }
            }
            */

            @media (min-width: 650px){
                .container {
                    max-width: 630px;
                }
            }

            @media (max-width: 1000px){
                .table-responsive {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                    -ms-overflow-style: -ms-autohiding-scrollbar;
                }
            }
        </style>
    </head>
    <body>
        <main>
            <div class="section container">
                <!--INÍCIO DO CABEÇALHO-->
                <div class="header tron">
                    <div class="bloco">
                    OpenCodingTool
                    </div>
                </div>
                <!--FIM DO CABEÇALHO-->

                <!--INÍCIO DO BANNER-->
                <div class="verde-caal">';
        $html .= '<div class="tron2" style="height: 100px;">';
        $html .= '<h2 style="padding: 10px;">
                            OpenCodingTool
                        </h2>';
        $html .= '</div>
                </div>
                <!--FIM DO BANNER-->

                <!--INÍCIO DO CARGA-->
                <div>
                    <div class="tron2">
                    <h5>Segue abaixo o link para redefinição da senha de usuário OpenCodingTool!</h5>';

//                    $numero_de_bytes = 12;
//                    $random = random_bytes($numero_de_bytes);
        $length = 12;
        $random = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, $length);

        $html .='<p><a href="http://opencodingtool.com.br/public/usuario/redefinirsenha/hash/' . bin2hex($random) . base64_encode($id) . '" target="_blank">Link para redefinição de senha</a></p>
                    </div>
                </div>
           <div>
                    <div class="tron2 text-center">
                        <h3>
                            OpenCodingTool
                            <br>
                        </h3>
                        <div class="tron3">
                                Esse e-mail foi gerado automaticamente. 
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>';
        //mail envia para o status
        //mailer envia para o email de fora

        $mailer->setBodyHtml($html);
        $mailer->setFrom('mauriciosescobar@gmail.com', 'OpenCodingTool');
        $mailer->addTo($email);
        $mailer->setSubject('Email Recebido - OpenCodingTool');
        $mailer->send($transport);
    }

    ///novo codificador no sistema
    public function sendEmail($dados, $senha, $avaliador)
    {
        $config = array('ssl' => 'tls',
            'auth' => 'login',
            'username' => 'mauriciosescobar@gmail.com',
//            'username' => 'b2b@caal.com.br',
            'port' => '587',
            'password' => 'hdtoknswuqdovoaf');
//            'password' => 'rxV$3Z2x');

//        $transport = new Zend_Mail_Transport_Smtp('mail.caal.com.br', $config);
        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
        
        $mailer = new Zend_Mail('utf-8');

        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//PT" "http://www.w3.org/TR/html4/strict.dtd">
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br">
    <head>
        <title>CAAL - </title>
        <meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/0e31f58d04.js" crossorigin="anonymous"></script>
        <style>
            .section{
                border: 1px #006746 solid;
                border-radius: 15px;
            }

            .container{
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            div{
                display: block;
            }

            .header{
                background-color: white;
            }

            .verde-caal{
                color: white;
                background-color: #2fa4e7;
                border-radius: 5px;
            }

            .verde{
                color: green;
            }

            h2{
                text-align: center;
                font-family: sans-serif;
                font-size: 30px;
            }

            .text-center{
                text-align: center;
                font-family: sans-serif;
                font-size: 14px;
            }

            p{
                text-align: justify;
                font-family: sans-serif;
            }

            .tron{
                /*padding: 1rem 2rem;*/
                padding: 1rem;
            }

            .tron1{
                padding: 4rem 2rem;
            }

            .tron2{
                padding: 1rem 2rem;
            }

            .tron2.h2{
                padding: 1rem 2rem;
            }

            .tron3{
                padding: 1rem 1rem;
            }

            .linha{
                margin-right: -15px;
                margin-left: -15px;
            }

            .bloco{
                margin-left: auto;
                margin-right: auto;
                text-align: center;
            }

            .table {
              width: 100%;
              margin-bottom: 1rem;
              color: #212529;
            }

            .table th,
            .table td {
              padding: 0.75rem;
              vertical-align: top;
              border-top: 1px solid #dee2e6;
            }

            .table thead th {
              vertical-align: bottom;
              border-bottom: 2px solid #dee2e6;
            }

            .table tbody + tbody {
              border-top: 2px solid #dee2e6;
            }

            .table-sm th,
            .table-sm td {
              padding: 0.3rem;
            }

            .table-bordered {
              border: 1px solid #dee2e6;
            }

            .table-bordered th,
            .table-bordered td {
              border: 1px solid #dee2e6;
            }

            .table-bordered thead th,
            .table-bordered thead td {
              border-bottom-width: 2px;
            }

            .table-borderless th,
            .table-borderless td,
            .table-borderless thead th,
            .table-borderless tbody + tbody {
              border: 0;
            }
            
            .fa-instagram{
                color: #f58529;
            }
            
            .fa-whatsapp{
                color: #075E54;
            }
            
            .fa-facebook{
                color: #3B5998;
            }

            /*
            @media (min-width: 1200px){
                .container{
                    max-width: 1140px;
                }
            }
            */

            @media (min-width: 650px){
                .container {
                    max-width: 630px;
                }
            }

            @media (max-width: 1000px){
                .table-responsive {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                    -ms-overflow-style: -ms-autohiding-scrollbar;
                }
            }
        </style>
    </head>
    <body>
        <main>
            <div class="section container">
                <!--INÍCIO DO CABEÇALHO-->
                <div class="header tron">
                    <div class="bloco">
                    OpenCodingTool
                    </div>
                </div>
                <!--FIM DO CABEÇALHO-->

                <!--INÍCIO DO BANNER-->
                <div class="verde-caal">';
        $html .= '<div class="tron2" style="height: 100px;">';
        $html .= '<h2 style="padding: 10px;">
                            OpenCodingTool
                        </h2>';
        $html .= '</div>
                </div>
                <!--FIM DO BANNER-->

                <!--INÍCIO DO CARGA-->
                <div>
                    <div class="tron2">
                    <h5>OpenCodingTool - Convite para Codificação</h5>
                    <p>Bem vindo, novo codificador!</p>
                    <p>Você acaba de receber o seu login de acesso ao sistema OpenCodingTool!</p>
                    <br>
                    <p>Dono do Projeto: ' . $avaliador . '</p>
                    <p>Email: ' . $dados['login'] . '</p>
                    <p>Senha: ' . $senha . '</p>
                    </div>
                </div>
           <div>
                    <div class="tron2 text-center">
                        <h3>
                            OpenCodingTool
                            <br>
                        </h3>
                        
                        <div class="tron3">
                                Esse e-mail foi gerado automaticamente. 
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>';
        //mail envia para o status
        //mailer envia para o email de fora

        $mailer->setBodyHtml($html);
        $mailer->setFrom('mauriciosescobar@gmail.com', 'OpenCodingTool');
        $mailer->addTo($dados['login']);
        $mailer->setSubject('Email Recebido - OpenCodingTool');
        $mailer->send($transport);
    }

    //codificador adicionado ao arquivo
    public function sendEmailAdicionado($dados, $avaliador)
    {
        $config = array('ssl' => 'tls',
            'auth' => 'login',
            'username' => 'mauriciosescobar@gmail.com',
//            'username' => 'b2b@caal.com.br',
            'port' => '587',
            'password' => 'hdtoknswuqdovoaf');
//            'password' => 'rxV$3Z2x');

//        $transport = new Zend_Mail_Transport_Smtp('mail.caal.com.br', $config);
        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
        
        $mailer = new Zend_Mail('utf-8');

        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//PT" "http://www.w3.org/TR/html4/strict.dtd">
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br">
    <head>
        <title>CAAL - </title>
        <meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/0e31f58d04.js" crossorigin="anonymous"></script>
        <style>
            .section{
                border: 1px #006746 solid;
                border-radius: 15px;
            }

            .container{
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            div{
                display: block;
            }

            .header{
                background-color: white;
            }

            .verde-caal{
                color: white;
                background-color: #2fa4e7;
                border-radius: 5px;
            }

            .verde{
                color: green;
            }

            h2{
                text-align: center;
                font-family: sans-serif;
                font-size: 30px;
            }

            .text-center{
                text-align: center;
                font-family: sans-serif;
                font-size: 14px;
            }

            p{
                text-align: justify;
                font-family: sans-serif;
            }

            .tron{
                /*padding: 1rem 2rem;*/
                padding: 1rem;
            }

            .tron1{
                padding: 4rem 2rem;
            }

            .tron2{
                padding: 1rem 2rem;
            }

            .tron2.h2{
                padding: 1rem 2rem;
            }

            .tron3{
                padding: 1rem 1rem;
            }

            .linha{
                margin-right: -15px;
                margin-left: -15px;
            }

            .bloco{
                margin-left: auto;
                margin-right: auto;
                text-align: center;
            }

            .table {
              width: 100%;
              margin-bottom: 1rem;
              color: #212529;
            }

            .table th,
            .table td {
              padding: 0.75rem;
              vertical-align: top;
              border-top: 1px solid #dee2e6;
            }

            .table thead th {
              vertical-align: bottom;
              border-bottom: 2px solid #dee2e6;
            }

            .table tbody + tbody {
              border-top: 2px solid #dee2e6;
            }

            .table-sm th,
            .table-sm td {
              padding: 0.3rem;
            }

            .table-bordered {
              border: 1px solid #dee2e6;
            }

            .table-bordered th,
            .table-bordered td {
              border: 1px solid #dee2e6;
            }

            .table-bordered thead th,
            .table-bordered thead td {
              border-bottom-width: 2px;
            }

            .table-borderless th,
            .table-borderless td,
            .table-borderless thead th,
            .table-borderless tbody + tbody {
              border: 0;
            }
            
            .fa-instagram{
                color: #f58529;
            }
            
            .fa-whatsapp{
                color: #075E54;
            }
            
            .fa-facebook{
                color: #3B5998;
            }

            /*
            @media (min-width: 1200px){
                .container{
                    max-width: 1140px;
                }
            }
            */

            @media (min-width: 650px){
                .container {
                    max-width: 630px;
                }
            }

            @media (max-width: 1000px){
                .table-responsive {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                    -ms-overflow-style: -ms-autohiding-scrollbar;
                }
            }
        </style>
    </head>
    <body>
        <main>
            <div class="section container">
                <!--INÍCIO DO CABEÇALHO-->
                <div class="header tron">
                    <div class="bloco">
                    OpenCodingTool
                    </div>
                </div>
                <!--FIM DO CABEÇALHO-->

                <!--INÍCIO DO BANNER-->
                <div class="verde-caal">';
        $html .= '<div class="tron2" style="height: 100px;">';
        $html .= '<h2 style="padding: 10px;">
                            OpenCodingTool
                        </h2>';
        $html .= '</div>
                </div>
                <!--FIM DO BANNER-->

                <!--INÍCIO DO CARGA-->
                <div>
                    <div class="tron2">
                    <p>Olá codificador!</p>
                    <br>
                    <p>Você acaba de ser adicionado ao arquivo ' . $dados[3] . '!</p>
                    <p>Arquivo que pertence ao projeto ' . $dados[4] . '!</p>
                    <p>Projeto de autoria de ' . $avaliador . ' !</p>
                    <br>
                    <p>Boa sorte!</p>
                    </div>
                </div>
           <div>
                    <div class="tron2 text-center">
                        <h3>
                            OpenCodingTool
                            <br>
                        </h3>
                        
                        <div class="tron3">
                                Esse e-mail foi gerado automaticamente. 
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>';
        //mail envia para o status
        //mailer envia para o email de fora

        $mailer->setBodyHtml($html);
        $mailer->setFrom('mauriciosescobar@gmail.com', 'OpenCodingTool');
        $mailer->addTo($dados[2]);
        $mailer->setSubject('Email Recebido - OpenCodingTool');
        $mailer->send($transport);
    }

}
