<?php

class Application_Model_DbTable_CodificadorTrecho extends Zend_Db_Table_Abstract {

    protected $_name = 'codificador_has_trecho';
    protected $_rowClass = "Application_Model_CodificadorTrecho";

    public function cadastrar($idcodificador, $idtrecho)
    {
        $codificadorTrecho = $this->createRow();
        /* @var $codificadorTrecho Application_Model_CodificadorTrecho */
        $codificadorTrecho->setIdcodificador(intval($idcodificador));
        $codificadorTrecho->setIdtrecho(intval($idtrecho));
        $codificadorTrecho->setStatus(0);

        return $codificadorTrecho->save();
    }

    public function finalizar($idtrecho)
    {
//        var_dump($id); die();
        $id = $this->getCodigoHasTrechoPorIdTrecho(intval($idtrecho));
//        var_dump($id[0]['idcodificadortrecho']); die();
        $this->find(intval($id[0]['idcodificadortrecho']))->current();
        $codificadorTrecho = $this->getCodigoHasTrechoId($id[0]['idcodificadortrecho']);
        
        /* @var $codificadorTrecho Application_Model_CodificadorTrecho */
        $codificadorTrecho->setStatus(1);

        return $codificadorTrecho->save();
    }
    
    public static function getCodigoHasTrechoPorIdTrecho($idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codificador_has_trecho.* FROM codificador_has_trecho WHERE"
                . " codificador_has_trecho.idtrecho = '$idtrecho'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodigoHasTrechoId($id)
    {
        $select = $this->select()->where('idcodificadortrecho = ?', $id);

        return $this->fetchRow($select);
    }

    public static function getCodigoHasTrechoPorId($idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codificador_has_trecho.*"
                . " FROM codificador_has_trecho WHERE"
                . " codificador_has_trecho.idtrecho = '$idtrecho'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function adicionar($idcodificador, $idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("UPDATE codificador_has_trecho SET status = 0 WHERE"
                . " codificador_has_trecho.idcodificador = '" . intval($idcodificador) . "' AND"
                . " codificador_has_trecho.idtrecho = '" . intval($idtrecho) . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function remover($idcodificador, $idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("UPDATE codificador_has_trecho SET status = 1 WHERE"
                . " codificador_has_trecho.idcodificador = '" . intval($idcodificador) . "' AND"
                . " codificador_has_trecho.idtrecho = '" . intval($idtrecho) . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
//        public function remover($idtrecho)
//    {
//        $id = $this->getCodigoHasTrechoPorIdTrecho(intval($idtrecho));
//        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();
//        $stmt = $adapter->query("DELETE FROM codificador_has_trecho WHERE codificador_has_trecho.idcodificadortrecho = '" . $id[0]['idcodificadortrecho'] . "'");
//
//        $rows = $stmt->fetchAll();
//
//        return $rows;
//    }

    public function getCodificadorPorIdTrecho($idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT usuario.login,"
                . " trecho.titulo as trecho, projeto.titulo as projeto FROM codificador_has_trecho,"
                . " codificador, usuario, trecho, projeto WHERE"
                . " codificador_has_trecho.idcodificador = codificador.idcodificador AND"
                . " codificador_has_trecho.idtrecho = trecho.idtrecho AND"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " codificador.idusuario = usuario.idusuario AND"
                . " codificador_has_trecho.idtrecho = '" . $idtrecho . "'");
//                . " UNION"
//                . " SELECT usuario.login as avaliador, trecho.titulo as trecho, projeto.titulo as projeto"
//                . " FROM usuario, trecho, projeto, avaliador WHERE"
//                . " projeto.idprojeto = trecho.idprojeto AND"
//                . " projeto.idavaliador = avaliador.idavaliador AND"
//                . " avaliador.idusuario = usuario.idusuario AND"
//                . " trecho.idtrecho = '" . $idtrecho . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodificadorTrecho($idtrecho, $status)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codificador.idcodificador, codificador_has_trecho.idtrecho"
                . " FROM codificador_has_trecho, codificador WHERE"
                . " codificador.idcodificador = codificador_has_trecho.idcodificador AND"
                . " codificador_has_trecho.status = '" . $status . "' AND"
                . " codificador_has_trecho.idtrecho = '" . $idtrecho . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function consultarCodificadorTrecho($idcodificador, $idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM codificador_has_trecho WHERE"
                . " codificador_has_trecho.idcodificador = '" . $idcodificador . "' AND"
                . " codificador_has_trecho.idtrecho = '" . $idtrecho . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function consultarCodificadorTrechoAdicionados($idcodificador, $idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM codificador_has_trecho WHERE"
                . " codificador_has_trecho.idcodificador = '" . $idcodificador . "' AND"
                . " codificador_has_trecho.status = '0' AND"
                . " codificador_has_trecho.idtrecho = '" . $idtrecho . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getTodosProjetosPorIdCodificador($idcodificador)
    {
//        var_dump($idcodificador);die();
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT(projeto.idprojeto), projeto.* FROM projeto, trecho, codificador, codificador_has_trecho WHERE"
                . " trecho.idprojeto = projeto.idprojeto AND"
                . " codificador_has_trecho.idcodificador = codificador.idcodificador AND"
                . " codificador_has_trecho.idtrecho = trecho.idtrecho AND"
                . " projeto.status = 1 AND"
                . " codificador_has_trecho.idcodificador = '" . $idcodificador . "' ORDER BY projeto.idprojeto DESC");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
        public function getTodosProjetosPorIdUsuario($idcodificador)
    {
//        var_dump($idcodificador);die();
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT projeto.* FROM projeto, usuario, codificador_has_trecho, codificador, trecho WHERE"
                . " codificador_has_trecho.idtrecho = trecho.idtrecho AND"
                . " codificador_has_trecho.idcodificador = codificador.idcodificador AND"
                . " trecho.idprojeto = projeto.idprojeto AND"
                . " usuario.idusuario = codificador.idusuario AND"
                . " codificador.idusuario = '" . $idcodificador . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodificadoresPorProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codificador.* FROM codificador, trecho, codificador_has_trecho, projeto WHERE"
                . " codificador_has_trecho.idcodificador = codificador.idcodificador AND"
                . " codificador_has_trecho.idtrecho = trecho.idtrecho AND"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " codificador_has_trecho.status = 0 AND"
                . " projeto.idprojeto = '" . $idprojeto . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

}
