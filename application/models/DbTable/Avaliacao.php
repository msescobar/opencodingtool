<?php

class Application_Model_DbTable_Avaliacao extends Zend_Db_Table_Abstract {

    protected $_name = 'avaliacao';
    protected $_rowClass = "Application_Model_Avaliacao";

    public function cadastrar($idusuario, $idtrecho)
    {
        $avaliacao = $this->createRow();
        /* @var $avaliacao Application_Model_Avaliacao */
        $avaliacao->setIdtrecho($idtrecho);
        $avaliacao->setIdusuario($idusuario);
        $avaliacao->setStatus(1);

        return $avaliacao->save();
    }

    public function statusConcluido($idavaliacao)
    {
        $this->find($idavaliacao)->current();
        $avaliacao = $this->getAvaliacaoPorId($idavaliacao);
        /* @var $avaliacao Application_Model_Avaliacao */

        $avaliacao->setStatus('0');

        return $avaliacao->save();
    }

    public function getAvaliacaoPorId($idavaliacao)
    {
        $select = $this->select()->where('idavaliacao = ?', $idavaliacao);

        return $this->fetchRow($select);
    }

    public function exibirAvaliacao($idusuario)
    {

//        include 'permissao.php';
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT avaliacao.status FROM avaliacao, usuario"
                . " WHERE usuario.idusuario = avaliacao.idusuario AND"
                . " usuario.idusuario = '$idusuario'");

//        var_dump($select->__toString());die();
        $rows = $stmt->fetchAll();

        return $rows;
    }

//    public function listarAvaliacao($idavaliador)
//    {
////        include 'permissao.php';
//        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();
//
//        $stmt = $adapter->query("Select DISTINCT usuario.idusuario"
//                . " FROM avaliacao, usuario, avaliador, trecho, projeto WHERE"
//                . " avaliacao.idusuario = usuario.idusuario AND"
//                . " projeto.idavaliador = '$idavaliador'");
//
//        $rows = $stmt->fetchAll();
//
//        return $rows;
//    }

    public function listarRelatorioCodigosAvaliacao($idavaliador)
    {
//        include 'permissao.php';
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT codigo.idcodigo, codigo.*, usuario.*,"
                . " trecho.descricao AS DESCRICAO, grupocodigo.descricao AS GRUPO FROM codigo, usuario, trecho,"
                . " projeto, grupocodigo WHERE "
                . " codigo.idgrupocodigo = grupocodigo.idgrupocodigo AND "
                . " codigo.idtrecho = trecho.idtrecho AND "
                . " codigo.idusuario = usuario.idusuario AND "
                . " codigo.status = '2' AND"
                . " projeto.idavaliador = '$idavaliador'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function downloadCsvAvaliacao()
    {
//        include 'permissao.php';
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $query = $adapter->query("SELECT usuario.login, codigo.descricao, codigo.idtrecho, codigo.status"
                . " FROM codigo, usuario WHERE "
                . "codigo.idusuario = usuario.idusuario");

        $rows = $query->fetchAll();

        return $rows;
    }

}
