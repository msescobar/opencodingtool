<?php

class Application_Model_DbTable_Codigo extends Zend_Db_Table_Abstract {

    protected $_name = 'codigo';
    protected $_rowClass = "Application_Model_Codigo";

    public function cadastrar($dados, $idusuario)
    {
        $codigo = $this->createRow();
        /* @var $codigo Application_Model_Codigo */
        $codigo->setIdusuario($idusuario);
        $codigo->setIdtrecho($dados['idtrecho']);
        $codigo->setDescricao($dados['codigo']);
        $codigo->setStatus(0);

        return $codigo->save();
    }

    public function cadastrarCodigoAceito($dados, $idusuario)
    {
        $codigo = $this->createRow();
        /* @var $codigo Application_Model_Codigo */
        $codigo->setIdusuario($idusuario);
        $codigo->setIdtrecho($dados['idtrecho']);
        $codigo->setDescricao($dados['descricao']);
        $codigo->setStatus(1);

        return $codigo->save();
    }

    public function exibirTrecho($idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT(trecho.titulo), trecho.idgrupoprojeto, trecho.idtrecho, trecho.descricao, grupoprojeto.descricao AS GRUPO"
                . " FROM codigo, trecho, grupoprojeto"
                . " WHERE trecho.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
                . " codigo.idtrecho = trecho.idtrecho AND codigo.idtrecho = '$idtrecho'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function listarTodosCodigos()
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codigo.*, grupocodigo.descricao as grupo FROM codigo, grupocodigo WHERE"
                . " codigo.idgrupocodigo = grupocodigo.idgrupocodigo");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getTrechoPorIdArquivo($idarquivo)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT trecho.*, projeto.titulo as projeto"
                . " FROM trecho, projeto WHERE"
                . " trecho.idprojeto = projeto.idprojeto AND"
                . " trecho.idtrecho = '$idarquivo'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodigosPorCodificador($idarquivo, $idcodificador)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT codigo.idcodigo, codigo.*, codificador_has_trecho.status as statuscodificacao"
                . " FROM codigo, trecho, usuario, codificador, codificador_has_trecho WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " usuario.idusuario = codigo.idusuario AND"
                . " codificador.idusuario = usuario.idusuario AND"
                . " codificador_has_trecho.idcodificador = codificador.idcodificador AND"
                . " codificador_has_trecho.idtrecho = trecho.idtrecho AND"
                . " codigo.status = 0 AND"
                . " codigo.idtrecho = '$idarquivo' AND"
                . " codificador.idcodificador = '$idcodificador'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodigosPorProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT codigo.* FROM projeto, codigo, trecho WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " trecho.idprojeto = projeto.idprojeto AND"
                . " codigo.status = 1 AND"
                . " projeto.idprojeto = '$idprojeto'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodigosAceitos($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT codigo.*, usuario.idperfil, grupocodigo.descricao as grupocodigo"
                . " FROM codigo, trecho, usuario, projeto, grupocodigohascodigo, grupocodigo WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " usuario.idusuario = codigo.idusuario AND"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " grupocodigohascodigo.idgrupocodigo = grupocodigo.idgrupocodigo AND"
                . " grupocodigo.idprojeto = projeto.idprojeto AND"
                . " codigo.status = 1 AND"
                . " trecho.idprojeto = '$idprojeto'");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
        public static function getCodigosAceitosAvaliar($idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT codigo.*, usuario.idperfil"
                . " FROM codigo, trecho, usuario, projeto WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " usuario.idusuario = codigo.idusuario AND"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " codigo.status = 1 AND"
                . " trecho.idtrecho = '$idtrecho'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodigosAceitosPorCodificador($idarquivo, $idcodificador)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codigo.*"
                . " FROM codigo, trecho, usuario, codificador WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " usuario.idusuario = codigo.idusuario AND"
                . " codificador.idusuario = usuario.idusuario AND"
                . " codigo.status = 1 AND"
                . " codigo.idtrecho = '$idarquivo' AND"
                . " codificador.idcodificador = '$idcodificador'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodigosAceitosPorAvaliador($idarquivo, $idusuario)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codigo.*"
                . " FROM codigo, trecho, usuario WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " usuario.idusuario = codigo.idusuario AND"
                . " codigo.status = 1 AND"
                . " codigo.idtrecho = '$idarquivo' AND"
                . " codigo.idusuario= '$idusuario'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodificadorPorArquivoListando($idarquivo)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codificador_has_trecho.idcodificador, codificador_has_trecho.idtrecho, usuario.login as nome FROM codificador_has_trecho, codificador, usuario WHERE"
                . " codificador_has_trecho.idcodificador = codificador.idcodificador AND"
                . " codificador.idusuario = usuario.idusuario AND"
                . " codificador_has_trecho.idtrecho = '$idarquivo'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodificadorPorArquivo($idarquivo)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT(codificador.idcodificador), codigo.idtrecho, codificador.idcodificador, usuario.login as nome "
                . " FROM codigo, trecho, usuario, codificador WHERE"
                . " codigo.idusuario = codificador.idusuario AND"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " usuario.idusuario = codigo.idusuario AND"
                . " codigo.idtrecho = '$idarquivo'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodificadorPorProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT codificador.*, projeto.idprojeto"
                . " FROM codigo, trecho, usuario, codificador, projeto WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " usuario.idusuario = codigo.idusuario AND"
                . " codificador.idusuario = usuario.idusuario AND"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " projeto.idprojeto = '$idprojeto'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodigoPorIdTrechoIdUsuario($idtrecho, $idusuario)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codigo.* FROM codigo, usuario, trecho WHERE"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " codigo.idusuario = usuario.idusuario AND"
                . " codigo.idtrecho = '$idtrecho' AND"
                . " codigo.idusuario = '$idusuario'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodigoPorIdGrupo($idgrupo)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codigo.* FROM codigo, grupocodigohascodigo WHERE"
                . " grupocodigohascodigo.idcodigo = codigo.idcodigo AND"
                . " grupocodigohascodigo.idgrupocodigo = '$idgrupo'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

//    public function listarTodosCodigos()
//    {
//        return $this->fetchAll();
//    }

    public function editarCodigo($dados)
    {
//        var_dump($dados); die();
//        $codigo = $this->find($id)->current();
        $codigo = $this->getCodigoPorId($dados['id']);
        /* @var $codigo Application_Model_Codigo */
        $codigo->setDescricao($dados['descricao']);
//        $codigo->setIdgrupocodigo($dados['idgrupocodigo']);

        return $codigo->save();
    }

    public function getCodigoPorId($id)
    {
        $select = $this->select()->where('idcodigo = ?', $id);

        return $this->fetchRow($select);
    }

    public function editarStatus($idcodigo, $status)
    {
        $this->find($idcodigo)->current();
        $codigo = $this->getCodigoPorId($idcodigo);
        /* @var $codigo Application_Model_Codigo */

        if ($status === '0') {
            $codigo->setStatus('1');
        } elseif ($status === '1') {
            $codigo->setStatus('3');
        }

        return $codigo->save();
    }

    public function statusAceito($idcodigo)
    {
        $this->find($idcodigo)->current();
        $codigo = $this->getCodigoPorId($idcodigo);
        /* @var $codigo Application_Model_Codigo */

        $codigo->setStatus('2');

        return $codigo->save();
    }

    public function statusNaoAceito($idcodigo)
    {
        $this->find($idcodigo)->current();
        $codigo = $this->getCodigoPorId($idcodigo);
        /* @var $codigo Application_Model_Codigo */

        $codigo->setStatus('3');

        return $codigo->save();
    }

    public function excluirCodigo($id)
    {
        $this->find($id)->current();
        $codigo = $this->getCodigoPorId($id);
        /* @var $codigo Application_Model_Codigo */

        $codigo->setStatus('0');

        return $codigo->save();
    }

    public function excluirCodigoAvaliador($id)
    {
        $this->find($id)->current();
        $codigo = $this->getCodigoPorId($id);
        /* @var $codigo Application_Model_Codigo */

        $codigo->setStatus('0');

        return $codigo->save();
    }

    public function aceitarCodigo($id)
    {
        $this->find($id)->current();
        $codigo = $this->getCodigoPorId($id);
        /* @var $codigo Application_Model_Codigo */

        $codigo->setStatus('1');

        return $codigo->save();
    }
    
        public function codigoAgrupado($idcodigo)
    {
        $this->find($idcodigo)->current();
        $codigo = $this->getCodigoPorId($idcodigo);
        /* @var $codigo Application_Model_Codigo */

        $codigo->setStatus('4');

        return $codigo->save();
    }

    public function excluirCodigoUsuario($id)
    {
        $select = $this->select()->where('idcodigo = ?', $id);
        $codigo = $this->fetchRow($select);

        return $codigo->delete();
    }
    
    public function getIdGrupoCodigoHasCodigo($idcodigo)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT grupocodigohascodigo.id FROM grupocodigohascodigo WHERE"
                . " grupocodigohascodigo.idcodigo = '$idcodigo'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

}
