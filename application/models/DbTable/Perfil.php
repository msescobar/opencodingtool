<?php

class Application_Model_DbTable_Perfil extends Zend_Db_Table_Abstract {

    protected $_name = 'perfil';
    protected $_rowClass = "Application_Model_Perfil";
    
    public function getPerfilPorId($id){
        $select = $this->select()->where('idperfil = ?',$id);
        
        return $this->fetchRow($select);
    }
}   