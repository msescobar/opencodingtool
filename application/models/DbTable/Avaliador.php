<?php

class Application_Model_DbTable_Avaliador extends Zend_Db_Table_Abstract
{

    protected $_name = 'avaliador';
    protected $_rowClass = "Application_Model_Avaliador";

    public function cadastrar($dados, $idusuario)
    {
        $avaliador = $this->createRow();
        /* @var $avaliador Application_Model_Avaliador */
        $avaliador->setIdusuario(intval($idusuario));
        $avaliador->setStatus(0);

        return $avaliador->save();
    }

    public function editar($id, $dados)
    {
        $usuario = $this->find($id)->current();
        /* @var $usuario Application_Model_Usuario */
        $usuario->setLogin($dados['login']);
//        $usuario->setSenha($dados['senha']);
//        $usuario->setIdgrupo($dados['idgrupo']);

        return $usuario->save();
    }
    
        public function excluir($id)
    {
        $select = $this->select()->where('idusuario = ?', $id);
        $usuario = $this->fetchRow($select);

        return $usuario->delete();
    }

    public function getAvaliadorPorIdUsuario($idusuario)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();
        
        $stmt = $adapter->query("SELECT avaliador.idavaliador, usuario.login FROM usuario, avaliador WHERE"
                . " usuario.idusuario = avaliador.idusuario AND"
                . " usuario.idusuario = '" . $idusuario . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
    public function getAvaliadorPorIdProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();
        
        $stmt = $adapter->query("SELECT usuario.login FROM avaliador, projeto, usuario WHERE"
                . " projeto.idavaliador = avaliador.idavaliador AND"
                . " usuario.idusuario = avaliador.idusuario AND"
                . " projeto.idprojeto = '" . $idprojeto . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

}
