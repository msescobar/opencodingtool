<?php

class Application_Model_DbTable_Grupocodigohascodigo extends Zend_Db_Table_Abstract {

    protected $_name = 'grupocodigohascodigo';
    protected $_rowClass = "Application_Model_Grupocodigohascodigo";

    public function cadastrar($dados)
    {
//        var_dump($dados); die();
        $grupo = $this->createRow();
        /* @var $grupo Application_Model_Grupocodigohascodigo */
        $grupo->setIdgrupocodigo(intval($dados['idgrupo']));
        $grupo->setIdcodigo(intval($dados['idcodigo']));

        return $grupo->save();
    }
    
        public static function getCodigosPorIdCategoria($idcategoria)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM grupocodigohascodigo WHERE"
                . " grupocodigohascodigo.idgrupocodigo = '$idcategoria'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function consultarCodigo($idcodigo)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM grupocodigohascodigo WHERE"
                . " grupocodigohascodigo.idcodigo = '$idcodigo'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function excluir($id)
    {
        $select = $this->select()->where('idcodigo = ?', $id);
        $codigo = $this->fetchRow($select);

        return $codigo->delete();
    }

    public function excluirCategoria($id)
    {
        $select = $this->select()->where('id = ?', $id);
        $grupocodigohascodigo = $this->fetchRow($select);

        return $grupocodigohascodigo->delete();
    }

}
