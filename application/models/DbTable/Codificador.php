<?php

class Application_Model_DbTable_Codificador extends Zend_Db_Table_Abstract {

    protected $_name = 'codificador';
    protected $_rowClass = "Application_Model_Codificador";

    public function cadastrar($idusuario, $idavaliador)
    {
        $codificador = $this->createRow();
        /* @var $codificador Application_Model_Codificador */
        $codificador->setIdusuario(intval($idusuario));
        $codificador->setIdavaliador(intval($idavaliador));
        $codificador->setStatus(0);

        return $codificador->save();
    }

    public function editar($id, $dados)
    {
        $codificador = $this->find($id)->current();
        /* @var $codificador Application_Model_Codificador */
        $codificador->setNome($dados['nome']);

        return $codificador->save();
    }

    public function excluir($id)
    {
        $select = $this->select()->where('idusuario = ?', $id);
        $usuario = $this->fetchRow($select);

        return $usuario->delete();
    }

    public function listarTodosCodificadoresPorAvaliador($idavaliador)
    {
//        include 'permissao.php';
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT usuario.* FROM codificador, usuario, avaliador WHERE"
                . " codificador.idusuario = usuario.idusuario AND"
                . " codificador.idavaliador = avaliador.idavaliador AND"
                . " usuario.idperfil = 3 AND"
                . " avaliador.idavaliador = '" . $idavaliador . "'");

//        var_dump($select->__toString());die();
        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodificadorPorIdUsuario($idusuario)
    {
//        include 'permissao.php';
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codificador.* FROM codificador, usuario WHERE"
                . " usuario.idusuario = codificador.idusuario AND"
                . " codificador.idusuario = '" . $idusuario . "'");

//        var_dump($select->__toString());die();
        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodificadorPorIdAvaliador($idavaliador)
    {
//        include 'permissao.php';
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codificador.*, usuario.login FROM codificador, usuario, avaliador WHERE"
                . " usuario.idusuario = codificador.idusuario AND"
                . " avaliador.idavaliador = codificador.idavaliador AND"
                . " avaliador.idavaliador = '" . $idavaliador . "'");

//        var_dump($select->__toString());die();
        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getCodificadoresPorIdAvaliador($idavaliador, $idtrecho, $status)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT codificador_has_trecho.* "
                . " FROM codificador, usuario, avaliador, codificador_has_trecho, trecho, projeto WHERE"
                . " usuario.idusuario = codificador.idusuario AND"
                . " avaliador.idavaliador = codificador.idavaliador AND"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " projeto.idavaliador = avaliador.idavaliador AND"
                . " codificador_has_trecho.idtrecho = trecho.idtrecho AND"
                . " avaliador.idavaliador = '" . $idavaliador . "' AND"
                . " codificador_has_trecho.idtrecho = '" . $idtrecho . "' AND"
                . " codificador_has_trecho.status = '" . $status . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
        public static function getLoginPorIdCodificador($idcodificador)
    {
//        include 'permissao.php';
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT usuario.login FROM codificador, usuario WHERE"
                . " usuario.idusuario = codificador.idusuario AND"
                . " codificador.idcodificador = '" . $idcodificador . "'");

//        var_dump($select->__toString());die();
        $rows = $stmt->fetchAll();

        return $rows;
    }

}
