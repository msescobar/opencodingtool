<?php

class Application_Model_DbTable_Projeto extends Zend_Db_Table_Abstract {

    protected $_name = 'projeto';
    protected $_rowClass = "Application_Model_Projeto";

    public function cadastrar($dados, $idavaliador)
    {
        $projeto = $this->createRow();
        /* @var $projeto Application_Model_Projeto */
        $projeto->setIdavaliador($idavaliador);
        $projeto->setTitulo($dados['titulo']);
        $projeto->setDescricao($dados['descricao']);
        $projeto->setRemovido(0);
        $projeto->setStart(0);
        $projeto->setStatus(0);
        $projeto->setCategorizar(0);
        $projeto->setAvaliar(0);

        return $projeto->save();
    }

    public function editar($dados)
    {
        $projeto = $this->find($dados['idprojeto'])->current();
        /* @var $projeto Application_Model_Projeto */
        $projeto->setTitulo($dados['titulo']);
        $projeto->setDescricao($dados['descricao']);

        return $projeto->save();
    }

    public function listarTodosProjetos()
    {
        return $this->fetchAll();
    }

    public function getProjetoPorIdProjeto($idgrupoprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT projeto.* FROM projeto, grupocodigo WHERE"
                . " projeto.idprojeto = grupocodigo.idprojeto AND"
                . " grupocodigo.idgrupocodigo = '" . $idgrupoprojeto . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getTodosProjetosPorIdAvaliador($idavaliador)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT projeto.* FROM projeto, avaliador WHERE"
                . " projeto.idavaliador = avaliador.idavaliador AND"
                . " projeto.removido = 0 AND"
                . " avaliador.idavaliador = '" . $idavaliador . "'"
                . " order by projeto.idprojeto DESC");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getTodosProjetosPorIdCodificador($idcodificador)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT projeto.* FROM projeto, "
                . "codificador, grupoprojeto, grupocodificadorprojeto WHERE"
                . " grupoprojeto.idprojeto = projeto.idprojeto AND"
                . " grupocodificadorprojeto.idcodificador = codificador.idcodificador AND"
                . " grupocodificadorprojeto.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
                . " codificador.idcodificador = '" . $idcodificador . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodigosCategorizadosPorIdProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT projeto.titulo, projeto.idprojeto, codigo.descricao, trecho.titulo as arquivo,"
                . " grupocodigo.descricao as categoria FROM projeto, grupocodigohascodigo, grupocodigo, codigo, trecho WHERE"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " grupocodigohascodigo.idcodigo = codigo.idcodigo AND"
                . " grupocodigohascodigo.idgrupocodigo = grupocodigo.idgrupocodigo AND"
                . " projeto.idprojeto = '" . $idprojeto . "' ORDER by categoria");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCodigosCategorizadosPorIdPj($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT projeto.titulo, codigo.descricao, trecho.titulo as arquivo,"
                . " grupocodigo.descricao as categoria FROM projeto, grupocodigohascodigo, grupocodigo, codigo, trecho WHERE"
                . " projeto.idprojeto = trecho.idprojeto AND"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " grupocodigohascodigo.idcodigo = codigo.idcodigo AND"
                . " grupocodigohascodigo.idgrupocodigo = grupocodigo.idgrupocodigo AND"
                . " projeto.idprojeto = '" . $idprojeto . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getProjetoPorIdProj($id)
    {
        $select = $this->select()->where('idprojeto = ?', $id);

        return $this->fetchRow($select);
    }

    public function getProjetoPorId($id)
    {
        $select = $this->select()->where('idprojeto = ?', $id);

        return $this->fetchRow($select);
    }

    public function statusConcluido($idprojeto)
    {
        $this->find($idprojeto)->current();
        $projeto = $this->getProjetoPorId($idprojeto);
        /* @var $projeto Application_Model_Projeto */

        $projeto->setStatus('1');

        return $projeto->save();
    }

    public function iniciar($idprojeto)
    {
        $this->find($idprojeto)->current();
        $projeto = $this->getProjetoPorId($idprojeto);
        /* @var $projeto Application_Model_Projeto */

        $projeto->setStart('1');

        return $projeto->save();
    }

    public function pausar($idprojeto)
    {
        $this->find($idprojeto)->current();
        $projeto = $this->getProjetoPorId($idprojeto);
        /* @var $projeto Application_Model_Projeto */

        $projeto->setStart('0');
        $projeto->setAvaliar('1');

        return $projeto->save();
    }

    public function finalizarAvaliacao($idprojeto)
    {
        $this->find($idprojeto)->current();
        $projeto = $this->getProjetoPorId($idprojeto);
        /* @var $projeto Application_Model_Projeto */

        $projeto->setAvaliar('0');
        $projeto->setCategorizar('1');

        return $projeto->save();
    }

    public function finalizarCategorizacao($idprojeto)
    {
        $this->find($idprojeto)->current();
        $projeto = $this->getProjetoPorId($idprojeto);
        /* @var $projeto Application_Model_Projeto */

        $projeto->setCategorizar('2');

        return $projeto->save();
    }

    public function remover($idprojeto)
    {
        $this->find($idprojeto)->current();
        $projeto = $this->getProjetoPorId($idprojeto);
        /* @var $projeto Application_Model_Projeto */

        $projeto->setRemovido('1');

        return $projeto->save();
    }

    public function getCodigosVinculadosAoProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT codigo.* FROM codigo, trecho, projeto WHERE"
                . " trecho.idprojeto = projeto.idprojeto AND"
                . " codigo.idtrecho = trecho.idtrecho AND"
                . " trecho.idprojeto = '" . $idprojeto . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getCategoriasVinculadasAoProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT grupocodigo.idgrupocodigo FROM grupocodigo, projeto WHERE"
                . " grupocodigo.idprojeto = projeto.idprojeto AND"
                . " grupocodigo.idprojeto = '" . $idprojeto . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getVerificarCategoriasVazias($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT * FROM grupocodigohascodigo WHERE"
                . " grupocodigohascodigo.idgrupocodigo = '" . $idprojeto . "'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function sendEmail($dados, $avaliador)
    {
//        var_dump($avaliador); die();
        $config = array('ssl' => 'tls',
            'auth' => 'login',
            'username' => 'mauriciosescobar@gmail.com',
//            'username' => 'b2b@caal.com.br',
            'port' => '587',
            'password' => 'hdtoknswuqdovoaf');
//            'password' => 'rxV$3Z2x');

//        $transport = new Zend_Mail_Transport_Smtp('mail.caal.com.br', $config);
        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

        $mailer = new Zend_Mail('utf-8');

        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//PT" "http://www.w3.org/TR/html4/strict.dtd">
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br">
    <head>
        <title>CAAL - </title>
        <meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/0e31f58d04.js" crossorigin="anonymous"></script>
        <style>
            .section{
                border: 1px #006746 solid;
                border-radius: 15px;
            }

            .container{
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            div{
                display: block;
            }

            .header{
                background-color: white;
            }

            .verde-caal{
                color: white;
                background-color: #2fa4e7;
                border-radius: 5px;
            }

            .verde{
                color: green;
            }

            h2{
                text-align: center;
                font-family: sans-serif;
                font-size: 30px;
            }

            .text-center{
                text-align: center;
                font-family: sans-serif;
                font-size: 14px;
            }

            p{
                text-align: justify;
                font-family: sans-serif;
            }

            .tron{
                /*padding: 1rem 2rem;*/
                padding: 1rem;
            }

            .tron1{
                padding: 4rem 2rem;
            }

            .tron2{
                padding: 1rem 2rem;
            }

            .tron2.h2{
                padding: 1rem 2rem;
            }

            .tron3{
                padding: 1rem 1rem;
            }

            .linha{
                margin-right: -15px;
                margin-left: -15px;
            }

            .bloco{
                margin-left: auto;
                margin-right: auto;
                text-align: center;
            }

            .table {
              width: 100%;
              margin-bottom: 1rem;
              color: #212529;
            }

            .table th,
            .table td {
              padding: 0.75rem;
              vertical-align: top;
              border-top: 1px solid #dee2e6;
            }

            .table thead th {
              vertical-align: bottom;
              border-bottom: 2px solid #dee2e6;
            }

            .table tbody + tbody {
              border-top: 2px solid #dee2e6;
            }

            .table-sm th,
            .table-sm td {
              padding: 0.3rem;
            }

            .table-bordered {
              border: 1px solid #dee2e6;
            }

            .table-bordered th,
            .table-bordered td {
              border: 1px solid #dee2e6;
            }

            .table-bordered thead th,
            .table-bordered thead td {
              border-bottom-width: 2px;
            }

            .table-borderless th,
            .table-borderless td,
            .table-borderless thead th,
            .table-borderless tbody + tbody {
              border: 0;
            }
            
            .fa-instagram{
                color: #f58529;
            }
            
            .fa-whatsapp{
                color: #075E54;
            }
            
            .fa-facebook{
                color: #3B5998;
            }

            /*
            @media (min-width: 1200px){
                .container{
                    max-width: 1140px;
                }
            }
            */

            @media (min-width: 650px){
                .container {
                    max-width: 630px;
                }
            }

            @media (max-width: 1000px){
                .table-responsive {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                    -ms-overflow-style: -ms-autohiding-scrollbar;
                }
            }
        </style>
    </head>
    <body>
        <main>
            <div class="section container">
                <!--INÍCIO DO CABEÇALHO-->
                <div class="header tron">
                    <div class="bloco">
                    OpenCodingTool
                    </div>
                </div>
                <!--FIM DO CABEÇALHO-->

                <!--INÍCIO DO BANNER-->
                <div class="verde-caal">';
        $html .= '<div class="tron2" style="height: 100px;">';
        $html .= '<h2 style="padding: 10px;">
                            OpenCodingTool
                        </h2>';
        $html .= '</div>
                </div>
                <!--FIM DO BANNER-->

                <!--INÍCIO DO CARGA-->
                <div>
                    <div class="tron2">
                    <h5>OpenCodingTool</h5>
                    <p>Você acaba de receber um email do sistema OpenCodingTool!</p>
                    <p>Você já pode iniciar a codificação no seguinte Arquivo:</p>
                    <br>
                    <p>Projeto: ' . $dados['projeto'] . '</p>
                    <p>Arquivo: ' . $dados['trecho'] . '</p>
                    </div>
                </div>
           <div>
                    <div class="tron2 text-center">
                        <h3>
                            OpenCodingTool
                            <br>
                        </h3>
                        
                        <div class="tron3">
                                Esse e-mail foi gerado automaticamente. 
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>';
        //mail envia para o status
        //mailer envia para o email de fora

        $mailer->setBodyHtml($html);
//        $mailer->setFrom('mauriciosescobar@gmail.com', 'OpenCodingTool');
        $mailer->setFrom($avaliador, 'OpenCodingTool');
//        $mailer->addTo($dados['login']);
        $mailer->addTo('mauriciosescobar@gmail.com');
        $mailer->setSubject('Email Recebido - OpenCodingTool');
        $mailer->send($transport);
    }

}
