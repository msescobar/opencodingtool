<?php

class Application_Model_DbTable_Trecho extends Zend_Db_Table_Abstract {

    protected $_name = 'trecho';
    protected $_rowClass = "Application_Model_Trecho";

    public function cadastrarTrecho($titulo, $idprojeto, $tipo)
    {
//        var_dump($tipo); die();
        $trecho = $this->createRow();
        /* @var $trecho Application_Model_Trecho */
        $trecho->setTitulo($titulo);
        $trecho->setIdprojeto(intval($idprojeto));
//        $trecho->setDescricao('');
        $trecho->setStatus(0);
        $trecho->setTipo($tipo);

        return $trecho->save();
    }

    public function listarTodosTrechos()
    {
        return $this->fetchAll();
    }

    public function alterarCaminhoFoto($caminho, $id)
    {
        $select = $this->select()->where('idtrecho = ?', $id);
        $trecho = $this->fetchRow($select);
        /* @var $trecho Application_Model_Trecho */
        $trecho->setDescricao($caminho);

        return $trecho->save();
    }

    public function getQuantidadeTrechos()
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT COUNT(trecho.idtrecho) as total FROM trecho");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getTrechoPorIdProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT trecho.* FROM trecho WHERE"
                . " trecho.idprojeto = '$idprojeto'");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
//    public static function getTrechosPorIdProjeto($idprojeto)
//    {
//        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();
//
//        $stmt = $adapter->query("SELECT DISTINCT trecho.*, projeto.idprojeto, codificador_has_trecho.status as codificacaostatus,"
//                . " projeto.descricao as projeto, projeto.status as projetostatus, projeto.start"
//                . " FROM trecho, projeto, codificador_has_trecho, codificador WHERE"
//                . " codificador_has_trecho.idcodificador = codificador.idcodificador AND"
//                . " codificador_has_trecho.idtrecho = codificador_has_trecho.idtrecho AND"
//                . " trecho.idprojeto = projeto.idprojeto AND"
//                . " trecho.status = 0 AND"
//                . " projeto.idprojeto = '$idprojeto'");
//
//        $rows = $stmt->fetchAll();
//
//        return $rows;
//    }
    
    public static function getTrechosPorIdProjeto($idprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT trecho.*, projeto.idprojeto,"
                . " projeto.descricao as projeto, projeto.status as projetostatus, projeto.start"
                . " FROM trecho, projeto WHERE"
                . " trecho.idprojeto = projeto.idprojeto AND"
                . " trecho.status = 0 AND"
                . " projeto.idprojeto = '$idprojeto'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function statusFinalizado($idtrecho)
    {
        $this->find($idtrecho)->current();
        $trecho = $this->getTrechoPorId($idtrecho);
        /* @var $trecho Application_Model_Trecho */

        $trecho->setStatus('1');

        return $trecho->save();
    }

    public function getTrechoPorId($id)
    {
        $select = $this->select()->where('idtrecho = ?', $id);

        return $this->fetchRow($select);
    }

    public static function getUsuarioTrechoGrupo($idusuario, $idtrecho, $idgrupoprojeto)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT DISTINCT trecho.titulo, trecho.descricao as DESCRICAO,"
                . " trecho.idtrecho, grupoprojeto.idgrupoprojeto, avaliacao.status,"
                . " IF(avaliacao.status <> 0,'false','true') as result FROM usuario, avaliacao, trecho, grupoprojeto WHERE"
                . " trecho.idtrecho = avaliacao.idtrecho AND"
                . " trecho.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
                . " avaliacao.idusuario = usuario.idusuario AND"
                . " avaliacao.idusuario = '$idusuario' AND"
                . " avaliacao.idtrecho = '$idtrecho' AND"
                . " trecho.idgrupoprojeto = '$idgrupoprojeto'");

        $rows = $stmt->fetchAll();

        return $rows;
    }

//        public static function getUsuarioTrechoGrupo($idusuario, $idtrecho, $idgrupo)
//    {
//        $adapter = Application_Model_DbTable_Grupoprojeto::getAdapterPersonalizado();
//
//        $stmt = $adapter->query("SELECT DISTINCT trecho.titulo, trecho.descricao as DESCRICAO,"
//                . " trecho.idtrecho, grupoprojeto.idgrupoprojeto, avaliacao.status,"
//                . " IF(avaliacao.status > 0,'false','true') as result FROM usuario, avaliacao, trecho, grupoprojeto WHERE"
//                . " trecho.idtrecho = avaliacao.idtrecho AND"
//                . " trecho.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
//                . " grupoprojeto.idgrupoprojeto = avaliacao.idgrupoprojeto AND"
//                . " avaliacao.idusuario = usuario.idusuario AND"
//                . " avaliacao.idusuario = '$idusuario' AND"
//                . " avaliacao.idtrecho = '$idtrecho' AND"
//                . " avaliacao.idgrupoprojeto = '$idgrupo'");
//
//        $rows = $stmt->fetchAll();
//
//        return $rows;
//    }

    public static function getUsuarioTrecho($idusuario, $idtrecho)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT avaliacao.status AS status FROM avaliacao, trecho, usuario"
                . " WHERE trecho.idtrecho = avaliacao.idtrecho AND"
                . " usuario.idusuario = avaliacao.idusuario AND"
                . " usuario.idusuario = '$idusuario' AND"
                . " trecho.idtrecho = '$idtrecho' ");

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public static function getTrechoPorIdProjetoIdCodificador($idprojeto, $idcodificador)
    {
        $adapter = Application_Model_DbTable_Usuario::getAdapterPersonalizado();

        $stmt = $adapter->query("SELECT trecho.*, avaliacao.status as avaliacao"
                . " FROM trecho, avaliacao, projeto, codificador, grupoprojeto, grupocodificadorprojeto WHERE"
                . " grupoprojeto.idprojeto = projeto.idprojeto AND"
                . " grupocodificadorprojeto.idcodificador = codificador.idcodificador AND"
                . " grupocodificadorprojeto.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
                . " trecho.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
                . " trecho.idtrecho = avaliacao.idtrecho AND"
                . " codificador.idusuario = avaliacao.idusuario AND"
                . " codificador.idcodificador = '$idcodificador' AND"
                . " projeto.idprojeto = '$idprojeto'");

        $rows = $stmt->fetchAll();

        return $rows;
    }
    
//    public function finalizar($idtrecho)
//    {
////        $this->find($idtrecho)->current();
//        $trecho = $this->getTrechoPorId(intval($idtrecho));
//        /* @var $trecho Application_Model_Trecho */
//
//        $trecho->setStatus('2');
//
//        return $trecho->save();
//    }
    
    public function remover($idtrecho)
    {
//        $this->find($idtrecho)->current();
        $trecho = $this->getTrechoPorId(intval($idtrecho));
        /* @var $trecho Application_Model_Trecho */

        $trecho->setStatus('1');

        return $trecho->save();
    }

//    public static function getTrechoPorIdProjetoIdCodificadorPDF($idprojeto, $idcodificador)
//    {
//        $adapter = Application_Model_DbTable_Grupoprojeto::getAdapterPersonalizado();
//
//        $stmt = $adapter->query("SELECT arquivo.* FROM arquivo, projeto, codificador, grupoprojeto, grupocodificadorprojeto WHERE"
//                . " grupoprojeto.idprojeto = projeto.idprojeto AND"
//                . " grupocodificadorprojeto.idcodificador = codificador.idcodificador AND"
//                . " grupocodificadorprojeto.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
//                . " arquivo.idgrupoprojeto = grupoprojeto.idgrupoprojeto AND"
//                . " codificador.idcodificador = '$idcodificador' AND"
//                . " projeto.idprojeto = '$idprojeto'");
//
//        $rows = $stmt->fetchAll();
//
//        return $rows;
//    }
}
