<?php

class Application_Model_Codificador extends Zend_Db_Table_Row_Abstract
{
//    private $idcodificador;
//    private $idusuario;
//    private $idavaliador;
//    private $status;
    
    function getIdcodificador()
    {
        return $this->idcodificador;
    }

    function getIdusuario()
    {
        return $this->idusuario;
    }

    function getIdavaliador()
    {
        return $this->idavaliador;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setIdcodificador($idcodificador)
    {
        $this->idcodificador = $idcodificador;
    }

    function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;
    }

    function setIdavaliador($idavaliador)
    {
        $this->idavaliador = $idavaliador;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

}