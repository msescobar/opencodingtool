<?php

class Application_Model_Avaliacao extends Zend_Db_Table_Row_Abstract
{

//    private $idtrecho;
//    private $idusuario;
//    private $status;
    
    function getIdtrecho()
    {
        return $this->idtrecho;
    }

    function getIdusuario()
    {
        return $this->idusuario;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setIdtrecho($idtrecho)
    {
        $this->idtrecho = $idtrecho;
    }

    function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }


    
}