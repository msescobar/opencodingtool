<?php

class Application_Model_Grupocodigohascodigo extends Zend_Db_Table_Row_Abstract
{
//    private $id;
//    private $idgrupocodigo;
//    private $idcodigo;
    
    function getId()
    {
        return $this->id;
    }

    function getIdgrupocodigo()
    {
        return $this->idgrupocodigo;
    }

    function getIdcodigo()
    {
        return $this->idcodigo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setIdgrupocodigo($idgrupocodigo)
    {
        $this->idgrupocodigo = $idgrupocodigo;
    }

    function setIdcodigo($idcodigo)
    {
        $this->idcodigo = $idcodigo;
    }
    
}