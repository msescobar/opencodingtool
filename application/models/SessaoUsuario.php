<?php

class Application_Model_SessaoUsuario extends Zend_Db_Table_Row_Abstract
{

    private $sessaoUsuario;

    function __construct()
    {
        $this->sessaoUsuario = new Zend_Session_Namespace('opencodingtool');
    }

    public function getSessao()
    {
        return $this->sessaoUsuario->dadosUsuario;
    }

    public function inserirDados($dados)
    {
        $this->sessaoUsuario->unsetAll();
        $this->sessaoUsuario->dadosUsuario = $dados;
    }

}
