-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Jun-2022 às 02:23
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bdtcc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao`
--

CREATE TABLE `avaliacao` (
  `idavaliacao` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idtrecho` int(11) NOT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliador`
--

CREATE TABLE `avaliador` (
  `idavaliador` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `avaliador`
--

INSERT INTO `avaliador` (`idavaliador`, `idusuario`, `nome`, `status`) VALUES
(1, 1, 'Andréa Sabedra Bordin', 0),
(2, 2, 'Aline Vieira de Mello', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `codificador`
--

CREATE TABLE `codificador` (
  `idcodificador` int(11) NOT NULL,
  `idavaliador` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `codificador`
--

INSERT INTO `codificador` (`idcodificador`, `idavaliador`, `idusuario`, `nome`, `status`) VALUES
(1, 1, 7, 'Moroni Correa', 0),
(2, 1, 8, 'Rafael', 0),
(3, 1, 9, 'Willian Medeiros', 0),
(4, 1, 10, 'Luiz Guilherme', 0),
(5, 1, 11, 'Daniel Souza', 0),
(6, 1, 12, 'teste', 0),
(7, 1, 13, 'Jean Jacques Rousseau', 0),
(8, 2, 14, 'fulano', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `codificador_has_trecho`
--

CREATE TABLE `codificador_has_trecho` (
  `idcodificadortrecho` int(11) NOT NULL,
  `idcodificador` int(11) NOT NULL,
  `idtrecho` int(11) NOT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `codificador_has_trecho`
--

INSERT INTO `codificador_has_trecho` (`idcodificadortrecho`, `idcodificador`, `idtrecho`, `status`) VALUES
(1, 1, 1, 0),
(2, 3, 1, 0),
(3, 1, 2, 1),
(4, 8, 5, 0),
(6, 8, 6, 0),
(7, 1, 4, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `codigo`
--

CREATE TABLE `codigo` (
  `idcodigo` int(11) NOT NULL,
  `idtrecho` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `codigo`
--

INSERT INTO `codigo` (`idcodigo`, `idtrecho`, `idusuario`, `descricao`, `status`) VALUES
(4, 1, 7, 'injúria', 1),
(6, 1, 0, 'legacy systems', 2),
(7, 1, 7, 'tristeza', 2),
(9, 1, 7, 'migrating', 2),
(10, 1, 9, 'management', 2),
(11, 2, 11, 'management', 1),
(12, 2, 11, 'migrating', 1),
(13, 2, 7, 'Trecho 124', 0),
(14, 2, 12, 'razão', 2),
(15, 2, 13, 'razão', 2),
(16, 1, 7, 'promessa', 0),
(17, 1, 7, 'Povos livres', 0),
(18, 1, 7, 'natureza', 1),
(19, 1, 1, 'juventude', 1),
(20, 1, 9, 'a alma resiste', 0),
(21, 1, 9, 'liberdade', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupocodificadorprojeto`
--

CREATE TABLE `grupocodificadorprojeto` (
  `idcodificadortrecho` int(11) NOT NULL,
  `idcodificador` int(11) NOT NULL,
  `idprojeto` int(11) NOT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `grupocodificadorprojeto`
--

INSERT INTO `grupocodificadorprojeto` (`idcodificadortrecho`, `idcodificador`, `idprojeto`, `status`) VALUES
(1, 1, 1, NULL),
(2, 2, 1, NULL),
(3, 3, 1, NULL),
(4, 5, 1, NULL),
(5, 4, 1, NULL),
(6, 5, 1, NULL),
(7, 7, 1, NULL),
(8, 7, 1, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupocodigo`
--

CREATE TABLE `grupocodigo` (
  `idgrupocodigo` int(11) NOT NULL,
  `idtrecho` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `grupocodigo`
--

INSERT INTO `grupocodigo` (`idgrupocodigo`, `idtrecho`, `descricao`, `status`) VALUES
(1, 1, 'grupo de código piloto', 0),
(2, 1, 'beta teste grupo do projeto 2', 0),
(3, 1, 'grupo do projeto 3', 0),
(4, 1, 'grupo de código novo do projeto 1', 0),
(5, 1, 'definição simples', 0),
(6, 2, 'interpretação', 0),
(7, 2, 'beta testes', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupocodigohascodigo`
--

CREATE TABLE `grupocodigohascodigo` (
  `id` int(11) NOT NULL,
  `idgrupocodigo` int(11) NOT NULL,
  `idcodigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `grupocodigohascodigo`
--

INSERT INTO `grupocodigohascodigo` (`id`, `idgrupocodigo`, `idcodigo`) VALUES
(1, 1, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
  `idperfil` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`idperfil`, `descricao`) VALUES
(1, 'admin'),
(2, 'avaliador'),
(3, 'codificador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

CREATE TABLE `projeto` (
  `idprojeto` int(11) NOT NULL,
  `idavaliador` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `removido` tinyint(2) DEFAULT NULL,
  `start` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto`
--

INSERT INTO `projeto` (`idprojeto`, `idavaliador`, `titulo`, `descricao`, `status`, `removido`, `start`) VALUES
(1, 1, 'Projeto Piloto 1', 'Projeto criado para testes', 1, 0, 1),
(2, 1, 'Novo Projeto Piloto para teste beta', 'Testes de projetos', 0, 0, 1),
(3, 2, 'Projeto Beta testes', 'Beta testes de Projetos', 0, 0, 1),
(4, 2, 'Projeto de Filosofia', 'Filosofo Jean Jacques Rousseau', 0, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `trecho`
--

CREATE TABLE `trecho` (
  `idtrecho` int(11) NOT NULL,
  `idprojeto` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `tipo` tinyint(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `trecho`
--

INSERT INTO `trecho` (`idtrecho`, `idprojeto`, `titulo`, `descricao`, `status`, `tipo`) VALUES
(1, 1, 'Relatos de um pensador', '/arquivos/1/1643084602.pdf', 0, 1),
(2, 1, 'declaracao-de-beneficio-2', '/arquivos/2/2643084602.pdf', 0, 1),
(4, 2, 'declaracao-de-beneficio-3', '/arquivos/4/1655257861.pdf', 0, 1),
(5, 3, 'declaracao-de-beneficio.pdf', '/arquivos/5/1655767430.pdf', 1, 1),
(6, 3, 'Documento sem título.pdf', '/arquivos/6/1655767564.pdf', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `idperfil` int(11) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idusuario`, `idperfil`, `login`, `senha`, `status`) VALUES
(0, 1, 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(1, 2, 'andrea', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(2, 2, 'aline', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(7, 3, 'moroni', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(8, 3, 'rafael', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(9, 3, 'willian', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(10, 3, 'luiz', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(11, 3, 'daniel', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(12, 3, 'alfa', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(13, 3, 'jean', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0),
(14, 3, 'fulano', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avaliacao`
--
ALTER TABLE `avaliacao`
  ADD PRIMARY KEY (`idavaliacao`),
  ADD KEY `fk_avaliacao_trecho1_idx` (`idtrecho`),
  ADD KEY `fk_avaliacao_usuario1_idx` (`idusuario`);

--
-- Indexes for table `avaliador`
--
ALTER TABLE `avaliador`
  ADD PRIMARY KEY (`idavaliador`),
  ADD KEY `fk_avaliador_usuario1_idx` (`idusuario`);

--
-- Indexes for table `codificador`
--
ALTER TABLE `codificador`
  ADD PRIMARY KEY (`idcodificador`),
  ADD KEY `fk_codificador_usuario1_idx` (`idusuario`),
  ADD KEY `fk_codificador_avaliador1_idx` (`idavaliador`);

--
-- Indexes for table `codificador_has_trecho`
--
ALTER TABLE `codificador_has_trecho`
  ADD PRIMARY KEY (`idcodificadortrecho`),
  ADD KEY `fk_codificadortrecho_codificador1_idx` (`idcodificador`),
  ADD KEY `fk_codificadortrecho_trecho1_idx` (`idtrecho`);

--
-- Indexes for table `codigo`
--
ALTER TABLE `codigo`
  ADD PRIMARY KEY (`idcodigo`),
  ADD KEY `fk_codigo_trecho1_idx` (`idtrecho`),
  ADD KEY `fk_codigo_usuario1_idx` (`idusuario`);

--
-- Indexes for table `grupocodificadorprojeto`
--
ALTER TABLE `grupocodificadorprojeto`
  ADD PRIMARY KEY (`idcodificadortrecho`),
  ADD KEY `fk_grupocodificadorprojeto_codificador1_idx` (`idcodificador`),
  ADD KEY `fk_grupocodificadorprojeto_projeto1_idx` (`idprojeto`);

--
-- Indexes for table `grupocodigo`
--
ALTER TABLE `grupocodigo`
  ADD PRIMARY KEY (`idgrupocodigo`),
  ADD KEY `fk_grupocodigo_trecho1_idx` (`idtrecho`);

--
-- Indexes for table `grupocodigohascodigo`
--
ALTER TABLE `grupocodigohascodigo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grupocodigoHasCodigo_grupocodigo1_idx` (`idgrupocodigo`),
  ADD KEY `fk_grupocodigoHasCodigo_codigo1_idx` (`idcodigo`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`idperfil`);

--
-- Indexes for table `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`idprojeto`),
  ADD KEY `fk_projeto_avaliador1_idx` (`idavaliador`);

--
-- Indexes for table `trecho`
--
ALTER TABLE `trecho`
  ADD PRIMARY KEY (`idtrecho`),
  ADD KEY `fk_trecho_projeto1_idx` (`idprojeto`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `fk_usuario_perfil1_idx` (`idperfil`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avaliacao`
--
ALTER TABLE `avaliacao`
  MODIFY `idavaliacao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `avaliador`
--
ALTER TABLE `avaliador`
  MODIFY `idavaliador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `codificador`
--
ALTER TABLE `codificador`
  MODIFY `idcodificador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `codificador_has_trecho`
--
ALTER TABLE `codificador_has_trecho`
  MODIFY `idcodificadortrecho` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `codigo`
--
ALTER TABLE `codigo`
  MODIFY `idcodigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `grupocodificadorprojeto`
--
ALTER TABLE `grupocodificadorprojeto`
  MODIFY `idcodificadortrecho` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `grupocodigo`
--
ALTER TABLE `grupocodigo`
  MODIFY `idgrupocodigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `grupocodigohascodigo`
--
ALTER TABLE `grupocodigohascodigo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
  MODIFY `idperfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projeto`
--
ALTER TABLE `projeto`
  MODIFY `idprojeto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `trecho`
--
ALTER TABLE `trecho`
  MODIFY `idtrecho` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `avaliacao`
--
ALTER TABLE `avaliacao`
  ADD CONSTRAINT `fk_avaliacao_trecho1` FOREIGN KEY (`idtrecho`) REFERENCES `trecho` (`idtrecho`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avaliacao_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `avaliador`
--
ALTER TABLE `avaliador`
  ADD CONSTRAINT `fk_avaliador_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `codificador`
--
ALTER TABLE `codificador`
  ADD CONSTRAINT `fk_codificador_avaliador1` FOREIGN KEY (`idavaliador`) REFERENCES `avaliador` (`idavaliador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_codificador_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `codificador_has_trecho`
--
ALTER TABLE `codificador_has_trecho`
  ADD CONSTRAINT `fk_codificadortrecho_codificador1` FOREIGN KEY (`idcodificador`) REFERENCES `codificador` (`idcodificador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_codificadortrecho_trecho1` FOREIGN KEY (`idtrecho`) REFERENCES `trecho` (`idtrecho`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `codigo`
--
ALTER TABLE `codigo`
  ADD CONSTRAINT `fk_codigo_trecho1` FOREIGN KEY (`idtrecho`) REFERENCES `trecho` (`idtrecho`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_codigo_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `grupocodificadorprojeto`
--
ALTER TABLE `grupocodificadorprojeto`
  ADD CONSTRAINT `fk_grupocodificadorprojeto_codificador1` FOREIGN KEY (`idcodificador`) REFERENCES `codificador` (`idcodificador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grupocodificadorprojeto_projeto1` FOREIGN KEY (`idprojeto`) REFERENCES `projeto` (`idprojeto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `grupocodigo`
--
ALTER TABLE `grupocodigo`
  ADD CONSTRAINT `fk_grupocodigo_trecho1` FOREIGN KEY (`idtrecho`) REFERENCES `trecho` (`idtrecho`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `grupocodigohascodigo`
--
ALTER TABLE `grupocodigohascodigo`
  ADD CONSTRAINT `fk_grupocodigoHasCodigo_codigo1` FOREIGN KEY (`idcodigo`) REFERENCES `codigo` (`idcodigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grupocodigoHasCodigo_grupocodigo1` FOREIGN KEY (`idgrupocodigo`) REFERENCES `grupocodigo` (`idgrupocodigo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `projeto`
--
ALTER TABLE `projeto`
  ADD CONSTRAINT `fk_projeto_avaliador1` FOREIGN KEY (`idavaliador`) REFERENCES `avaliador` (`idavaliador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trecho`
--
ALTER TABLE `trecho`
  ADD CONSTRAINT `fk_trecho_projeto1` FOREIGN KEY (`idprojeto`) REFERENCES `projeto` (`idprojeto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_perfil1` FOREIGN KEY (`idperfil`) REFERENCES `perfil` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
