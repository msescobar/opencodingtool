<?php

class Application_Model_Relatorioa {

    public function criarRelatorioa($caminho_base, $carga, $pedidos)
    {

        $html = "<html>
<head>
<meta charset=utf-8'/>

<style>
.row {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}

.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
.col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
.col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
.col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
.col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
.col-xl-auto {
  position: relative;
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
}

.col {
  -ms-flex-preferred-size: 0;
  flex-basis: 0;
  -ms-flex-positive: 1;
  flex-grow: 1;
  max-width: 100%;
}

.col-4 {
  -ms-flex: 0 0 33.333333%;
  flex: 0 0 33.333333%;
  max-width: 33.333333%;
}

.col-6 {
  -ms-flex: 0 0 50%;
  flex: 0 0 50%;
  max-width: 50%;
}

.jumbotron {
  padding: 2rem 1rem;
  margin-bottom: 2rem;
  
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, 0.225);
  border-radius: 0.25rem;
}


.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
}

.table th,
.table td {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid #dee2e6;
}

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #dee2e6;
}

.table tbody + tbody {
  border-top: 2px solid #dee2e6;
}

.table-sm th,
.table-sm td {
  padding: 0.3rem;
}

.table-bordered {
  border: 1px solid #dee2e6;
}

.table-bordered th,
.table-bordered td {
  border: 1px solid #dee2e6;
}

.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 2px;
}

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
  color: #212529;
  background-color: rgba(0, 0, 0, 0.075);
}

.table-primary,
.table-primary > th,
.table-primary > td {
  background-color: #b8daff;
}

.table-primary th,
.table-primary td,
.table-primary thead th,
.table-primary tbody + tbody {
  border-color: #7abaff;
}

.table-hover .table-primary:hover {
  background-color: #9fcdff;
}

.table-hover .table-primary:hover > td,
.table-hover .table-primary:hover > th {
  background-color: #9fcdff;
}

.table-secondary,
.table-secondary > th,
.table-secondary > td {
  background-color: #d6d8db;
}

.table-secondary th,
.table-secondary td,
.table-secondary thead th,
.table-secondary tbody + tbody {
  border-color: #b3b7bb;
}

.table-hover .table-secondary:hover {
  background-color: #c8cbcf;
}

.table-hover .table-secondary:hover > td,
.table-hover .table-secondary:hover > th {
  background-color: #c8cbcf;
}

.table-success,
.table-success > th,
.table-success > td {
  background-color: #c3e6cb;
}

.table-success th,
.table-success td,
.table-success thead th,
.table-success tbody + tbody {
  border-color: #8fd19e;
}

.table-hover .table-success:hover {
  background-color: #b1dfbb;
}

.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #b1dfbb;
}

.table-info,
.table-info > th,
.table-info > td {
  background-color: #bee5eb;
}

.table-info th,
.table-info td,
.table-info thead th,
.table-info tbody + tbody {
  border-color: #86cfda;
}

.table-hover .table-info:hover {
  background-color: #abdde5;
}

.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #abdde5;
}

.table-warning,
.table-warning > th,
.table-warning > td {
  background-color: #ffeeba;
}

.table-warning th,
.table-warning td,
.table-warning thead th,
.table-warning tbody + tbody {
  border-color: #ffdf7e;
}

.table-hover .table-warning:hover {
  background-color: #ffe8a1;
}

.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #ffe8a1;
}

.table-danger,
.table-danger > th,
.table-danger > td {
  background-color: #f5c6cb;
}

.table-danger th,
.table-danger td,
.table-danger thead th,
.table-danger tbody + tbody {
  border-color: #ed969e;
}

.table-hover .table-danger:hover {
  background-color: #f1b0b7;
}

.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #f1b0b7;
}

.table-light,
.table-light > th,
.table-light > td {
  background-color: #fdfdfe;
}

.table-light th,
.table-light td,
.table-light thead th,
.table-light tbody + tbody {
  border-color: #fbfcfc;
}

.table-hover .table-light:hover {
  background-color: #ececf6;
}

.table-hover .table-light:hover > td,
.table-hover .table-light:hover > th {
  background-color: #ececf6;
}

.table-dark,
.table-dark > th,
.table-dark > td {
  background-color: #c6c8ca;
}

.table-dark th,
.table-dark td,
.table-dark thead th,
.table-dark tbody + tbody {
  border-color: #95999c;
}

.table-hover .table-dark:hover {
  background-color: #b9bbbe;
}

.table-hover .table-dark:hover > td,
.table-hover .table-dark:hover > th {
  background-color: #b9bbbe;
}

.table-active,
.table-active > th,
.table-active > td {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: rgba(0, 0, 0, 0.075);
}

.table .thead-dark th {
  color: #fff;
  background-color: #343a40;
  border-color: #454d55;
}

.table .thead-light th {
  color: #495057;
  background-color: #e9ecef;
  border-color: #dee2e6;
}

.table-dark {
  color: #fff;
  background-color: #343a40;
}

.table-dark th,
.table-dark td,
.table-dark thead th {
  border-color: #454d55;
}

.table-dark.table-bordered {
  border: 0;
}

.table-dark.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(255, 255, 255, 0.05);
}

.table-dark.table-hover tbody tr:hover {
  color: #fff;
  background-color: rgba(255, 255, 255, 0.075);
}

</style>
</head>
<body>
<img src='http://localhost" . $caminho_base . 'dist/img/logo.png' . "' style='width:80px'>
<div class='jumbotron' style='height:1300px; font-size: 11px; margin-top: 2%'>";
        $html.="<div style='width: 33.333333%; float:left;'>";
        $html.="<h2 style='margin-top:-10px; font-size: 12px;'>Carga nº: " . $carga[0]['id'] . "</h2>";
        $html.="<label><strong>Representante: </strong></label><label>" . $carga[0]['nome'] . "</label><br>";
        $html.="<label><strong>Transportadora: </strong></label><label>" . $carga[0]['razao_social'] . "</label><br>";
        $html.="</div>";

        $html.="<div style='width: 33.333333%; float:left;'>";
        $html.="<label><strong>Base Frete: </strong></label><label>" . $carga[0]['descricao'] . "</label><br>";
        $html.="<label><strong>Data do carregamento: </strong></label><label>" . date("d/m/Y", strtotime($carga[0]['data_carregamento'])) . "</label><br>";
        $html.="<label><strong>Qtd. total de fardos: </strong></label><label>" . $carga[0]['quantidade_total_fardos'] . "</label>";
        $html.="</div>";

        $html.="<div style='width: 33.333333%; float: left;'>";
        $html.="<label><strong>Valor total: </strong></label><label>R$ " . $carga[0]['valor_total_liquido_fardos'] . "</label><br>";
        $html.="<label><strong>Valor total dos fretes: </strong></label><label>R$ " . $carga[0]['valor_total_frete'] . "</label><br>";
        $html.="</div><label><strong>Observação: </strong></label><label>" . $carga[0]['observacao'] . "</label><br><br>";

        $html.="<div class='row'>";
        $html.="<div class='col'>";
        for ($i = 0; $i < sizeof($pedidos); $i++) {
            $html.="<div class='jumbotron' style='font-size: 11px; margin-bottom: 2px;'>";
            $html.="<div style='width: 50%; float:left;'>";
            $html.="<h2 style='margin-top:-12px; font-size: 10px'>Pedido nº: teste</h2>";
            $html.="<label><strong>Cliente/CNPJ: </strong></label><label>teste</label><br>";
            $html.="<label><strong>Matrícula: </strong></label><label>teste</label><br>";
            $html.="<label><strong>Rede: </strong></label><label>teste</label><br>";
            $html.="<table class='table table-bordered' style='font-size:11px; margin-bottom: 0px;'>
    <thead>
    <tr>
        <th style='width:200px; padding:0px;'>Produto</th>
        <th style='width:80px; padding:0px; text-align: center'>Quantidade</th>
        <th style='width:80px; padding:0px; text-align: center'>Desconto</th>
        <th style='width:80px; padding:0px; text-align: center'>Valor Unit. Frete</th>
        <th style='width:80px; padding:0px; text-align: center'>Valor Unit. Fardo</th>
        <th style='width:80px; padding:0px; text-align: center'>Valor Líquido</th>
    </tr>
</thead>
<tbody>";
            $dbTableItem = new Application_Model_DbTable_Item();
            $itens = $dbTableItem->getItemPorHash($pedidos[$i]['hash']);
            for ($k = 0; $k < sizeof($itens); $k++) {
                $html .= '<tr>'
                        . '<td style="padding:0px;">' . $itens[$k]['produto'] . '/' . $itens[$k]['tipo'] . '/' . $itens[$k]['embalagem'] . '</td>'
                        . '<td style="padding:0px; text-align: center">' . $itens[$k]['quantidade_fardos'] . '</td>'
                        . '<td style="padding:0px; text-align: center">' . $itens[$k]['desconto'] . '</td>'
                        . '<td style="padding:0px; text-align: center">' . $itens[$k]['valor_unitario_frete'] . '</td>'
                        . '<td style="padding:0px; text-align: center">' . $itens[$k]['valor_unitario_fardo'] . '</td>'
                        . '<td style="padding:0px; text-align: center">' . $itens[$k]['valor_total_fardos_liquido_unitario'] . '</td>'
                        . '</tr>';
            }
            $html.="</tbody>
        </table>
        </div>";
        }
        $html.="
</div>
</div>
        </div>";
        $html.="
</body>
</html>";
//        var_dump($html);die();
        return $html;
    }

}
