<?php

class Application_Model_Trecho extends Zend_Db_Table_Row_Abstract {

//    private $idtrecho;
//    private $idprojeto;
//    private $titulo;
//    private $descricao;
//    private $status;
//    private $tipo;
    
    function getIdtrecho()
    {
        return $this->idtrecho;
    }

    function getIdprojeto()
    {
        return $this->idprojeto;
    }

    function getTitulo()
    {
        return $this->titulo;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function getStatus()
    {
        return $this->status;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setIdtrecho($idtrecho)
    {
        $this->idtrecho = $idtrecho;
    }

    function setIdprojeto($idprojeto)
    {
        $this->idprojeto = $idprojeto;
    }

    function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

}