<?php

class Application_Model_Avaliador extends Zend_Db_Table_Row_Abstract
{
//    private $idavaliador;
//    private $idusuario;
//    private $status;
    
    function getIdavaliador()
    {
        return $this->idavaliador;
    }

    function getIdusuario()
    {
        return $this->idusuario;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setIdavaliador($idavaliador)
    {
        $this->idavaliador = $idavaliador;
    }

    function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }



}

