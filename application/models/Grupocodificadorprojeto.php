<?php

class Application_Model_Grupocodificadorprojeto extends Zend_Db_Table_Row_Abstract
{
    
//    private $idgrupocodificadorprojeto;
//    private $idgrupoprojeto;
//    private $idcodificador;
    
    function getIdgrupocodificadorprojeto()
    {
        return $this->idgrupocodificadorprojeto;
    }

    function getIdgrupoprojeto()
    {
        return $this->idgrupoprojeto;
    }

    function getIdcodificador()
    {
        return $this->idcodificador;
    }

    function setIdgrupocodificadorprojeto($idgrupocodificadorprojeto)
    {
        $this->idgrupocodificadorprojeto = $idgrupocodificadorprojeto;
    }

    function setIdgrupoprojeto($idgrupoprojeto)
    {
        $this->idgrupoprojeto = $idgrupoprojeto;
    }

    function setIdcodificador($idcodificador)
    {
        $this->idcodificador = $idcodificador;
    }


}

