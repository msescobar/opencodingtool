<?php

class Application_Model_ImagemVideo {

    private $caminhoTemp;
    private $caminhoDestino;
    private $novoNomeImagem;
    private $identidade;

    public function getIdentidade()
    {
        return $this->identidade;
    }

    public function setIdentidade($identidade)
    {
        $this->identidade = $identidade;
    }

    public function getCaminhoTemp()
    {
        return $this->caminhoTemp;
    }

    public function setCaminhoTemp($caminhoTemp)
    {
        $this->caminhoTemp = $caminhoTemp;
    }

    public function getCaminhoDestino()
    {
        return $this->caminhoDestino;
    }

    public function setCaminhoDestino($caminhoDestino)
    {
        $this->caminhoDestino = $caminhoDestino;
    }

    public function getNovoNomeImagem()
    {
        return $this->novoNomeImagem;
    }

    public function setNovoNomeImagem($novoNomeImagem)
    {
        $this->novoNomeImagem = $novoNomeImagem;
    }

    public function moverArquivoCadastrado($arquivo, $tipo)
    {
        $modelDiretorio = new Application_Model_Diretorio();
        $id = $arquivo['id'];

        $modelDiretorio->criarDir($id, 'arquivos');
        $this->setCaminhoDestino(PUBLIC_PATH . '/arquivos/' . $id . '/' . $arquivo['arquivo']['name']);
        $bool = move_uploaded_file($this->getCaminhoTemp(), $this->getCaminhoDestino());

        if ($bool) {
            $date = new DateTime();
            $nome = $date->getTimestamp();

            if ($tipo === 'pdf') {
                $boolRename = rename(PUBLIC_PATH . '/arquivos/' . $id . '/' . $arquivo['arquivo']['name'], PUBLIC_PATH . '/arquivos/' . $id . '/' . $nome . '.pdf');
   
                } else if ($tipo === 'docx') {
                $boolRename = rename(PUBLIC_PATH . '/arquivos/' . $id . '/' . $arquivo['arquivo']['name'], PUBLIC_PATH . '/arquivos/' . $id . '/' . $nome . '.docx');
    
                } else if ($tipo === 'doc') {
                $boolRename = rename(PUBLIC_PATH . '/arquivos/' . $id . '/' . $arquivo['arquivo']['name'], PUBLIC_PATH . '/arquivos/' . $id . '/' . $nome . '.doc');
            }

            if ($boolRename) {
                if ($tipo === 'pdf') {
                    return '/arquivos/' . $id . '/' . $nome . '.pdf';
                    
                } else if ($tipo === 'docx') {
                    return '/arquivos/' . $id . '/' . $nome . '.docx';
                    
                } else if ($tipo === 'doc') {
                    return '/arquivos/' . $id . '/' . $nome . '.doc';
                }
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

}
