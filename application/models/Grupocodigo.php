<?php

class Application_Model_Grupocodigo extends Zend_Db_Table_Row_Abstract
{
//    private $idgrupocodigo;
//    private $idprojeto;
//    private $descricao;
//    private $status;
    
    function getIdgrupocodigo()
    {
        return $this->idgrupocodigo;
    }

    function getIdprojeto()
    {
        return $this->idprojeto;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setIdgrupocodigo($idgrupocodigo)
    {
        $this->idgrupocodigo = $idgrupocodigo;
    }

    function setIdprojeto($idprojeto)
    {
        $this->idprojeto = $idprojeto;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }


    
}