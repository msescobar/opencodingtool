<?php

class Application_Model_Projeto extends Zend_Db_Table_Row_Abstract
{

//    private $idprojeto;
//    private $idavaliador;
//    private $titulo;
//    private $descricao;
//    private $status;
//    private $start;
//    private $removido;
//    private $categorizar;
//    private $avaliar;
    
    function getIdprojeto()
    {
        return $this->idprojeto;
    }

    function getIdavaliador()
    {
        return $this->idavaliador;
    }

    function getTitulo()
    {
        return $this->titulo;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function getStatus()
    {
        return $this->status;
    }

    function getStart()
    {
        return $this->start;
    }

    function getRemovido()
    {
        return $this->removido;
    }

    function getCategorizar()
    {
        return $this->categorizar;
    }

    function getAvaliar()
    {
        return $this->avaliar;
    }

    function setIdprojeto($idprojeto)
    {
        $this->idprojeto = $idprojeto;
    }

    function setIdavaliador($idavaliador)
    {
        $this->idavaliador = $idavaliador;
    }

    function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function setStart($start)
    {
        $this->start = $start;
    }

    function setRemovido($removido)
    {
        $this->removido = $removido;
    }

    function setCategorizar($categorizar)
    {
        $this->categorizar = $categorizar;
    }

    function setAvaliar($avaliar)
    {
        $this->avaliar = $avaliar;
    }

}