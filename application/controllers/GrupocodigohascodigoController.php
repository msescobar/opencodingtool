<?php

class GrupocodigohascodigoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
        
    }

    public function cadastrarAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view->layout()->disableLayout();

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
            
            $dbTableGrupocodigohascodigo = new Application_Model_DbTable_Grupocodigohascodigo();
            $resultConsulta = $dbTableGrupocodigohascodigo->consultarCodigo(intval($dados['idcodigo']));
            
            if(empty($resultConsulta)){
                $idgrupo = $dbTableGrupocodigohascodigo->cadastrar($dados);
                
            } else {
                $dbTableGrupocodigohascodigo->excluir($dados['idcodigo']);
                $idgrupo = $dbTableGrupocodigohascodigo->cadastrar($dados);
            }
                        
            if ($idgrupo != NULL) {
                $dbTableCodigo = new Application_Model_DbTable_Codigo();
                $dbTableCodigo->codigoAgrupado(intval($dados['idcodigo']));
                
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
//            }
        }
    }

}
