<?php

class AdminCodificadorController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin-codificador');
    }

    public function indexAction()
    {
        $this->_helper->layout->setLayout('layout_admin-codificador');
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        
        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
        $dbTableCodificador = new Application_Model_DbTable_Codificador();
        $dbTableCodificadorProjeto = new Application_Model_DbTable_CodificadorTrecho();
        
//        var_dump($idusuario); die();
        $codificador = $dbTableCodificador->getCodificadorPorIdUsuario(intval($idusuario));
        //vários resultados de codificador podem vir aqui!
//        var_dump($codificador[1]['idusuario']); die();
        
//        $this->view->codificador = $codificador;
        
        //for aqui, cadê? Vários codificadores podem estar aqui
        $codificadorprojetos = $dbTableCodificadorProjeto->getTodosProjetosPorIdUsuario(intval($codificador[0]['idusuario']));
//        var_dump($codificadorprojetos); die();
        
        $this->view->listaProjetos = $codificadorprojetos;
    }

    public function cadastrarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->view->message = '';

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados); die();
            $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
            $dbTableCodigo = new Application_Model_DbTable_Codigo();
            $id = $dbTableCodigo->cadastrar($dados, $idusuario);

            if ($id != NULL) {
                $this->view->message = "Cadastro realizada com sucesso!";
            } else {
                $this->view->message = "Não foi possível efetuar o cadastro!";
            }
        }
    }

    public function editarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        try {
            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();
//                var_dump($dados); die();

                $dbTableCodigo = new Application_Model_DbTable_Codigo();
                $result = $dbTableCodigo->editarCodigo($dados);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function trechosAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/codigo.js'));

        $idprojeto = $this->getRequest()->getParam('id');

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
        $dbTableCodificador = new Application_Model_DbTable_Codificador();
        $codificador = $dbTableCodificador->getCodificadorPorIdUsuario($idusuario);

        $dbTableTrecho = new Application_Model_DbTable_Trecho();
        $dbTableGrupoCodigo = new Application_Model_DbTable_GrupoCodigo();
        $trechos = $dbTableTrecho->getTrechoPorIdProjetoIdCodificador($idprojeto, $codificador['idcodificador']);
        $gruposcodigos = $dbTableGrupoCodigo->getGrupoCodigosPorIdProjeto($idprojeto);

        $this->view->listaDosGruposCodigos = $gruposcodigos;
        $this->view->listaDosTrechos = $trechos;
    }

    public function codificarAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/codigo.js'));

        $dbTableCodigo = new Application_Model_DbTable_Codigo();

        $idtrecho = $this->getRequest()->getParam('id');
        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;

        $codigos = $dbTableCodigo->getCodigoPorIdTrechoIdUsuario($idtrecho, $idusuario);
        $trecho = $dbTableCodigo->getTrechoPorIdArquivo($idtrecho);

        $this->view->arquivo = $trecho;
        $this->view->listaDosCodigos = $codigos;
    }

    public function finalizarCodificacaoAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/codigo.js'));

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados); die();
//            $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;

            $dbTableCodificadorTrecho = new Application_Model_DbTable_CodificadorTrecho();
//            $dbTableCodificador = new Application_Model_DbTable_Codificador();

//            $codificador = $dbTableCodificador->getCodificadorPorIdUsuario($idusuario);
//            $result = $dbTableCodificadorTrecho->getCodigoHasTrechoPorId($dados['idtrecho'], $codificador[0]['idcodificador']);
//            $finalizar = $dbTableCodificadorTrecho->finalizar($result[0]['idcodificadortrecho'], $dados['idtrecho'], $codificador[0]['idcodificador']);
            $finalizar = $dbTableCodificadorTrecho->finalizar($dados['idtrecho']);

            if ($finalizar !== NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }
    }

    public function listartrechosAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $this->_helper->layout->setLayout('layout_avaliacao');

        $dbTableGrupo = new Application_Model_DbTable_Grupo();
        $grupo = $dbTableGrupo->listarTodosGrupos();
        $this->view->listaDosGrupos = $grupo;

        $idgrupo = $this->getRequest()->getParam('id');
        $trechos = $dbTableGrupo->listarTrechosPorGrupo($idgrupo);

        $this->view->listaDosTrechosPorGrupo = $trechos;
        $this->view->getGrupoPorId = $idgrupo;
    }

    public function listarcodigosAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $this->_helper->layout->setLayout('layout_avaliacao');

        $dbTableGrupo = new Application_Model_DbTable_Grupo();
        $grupo = $dbTableGrupo->listarTodosGrupos();
        $this->view->listaDosGrupos = $grupo;

        $ids = $this->getRequest()->getParam('id');
        $dados = explode(',', $ids);
        $idgrupo = $dados['0'];
        $idtrecho = $dados['1'];

        $trechos = $dbTableGrupo->listarTrechosPorGrupo($idgrupo);
        $usuarios = $dbTableGrupo->listarUsuariosPorGrupo($idgrupo, $idtrecho);
        $codigos = $dbTableGrupo->listarCodigosPorTrecho($idgrupo, $idtrecho);
        $trecho = $dbTableGrupo->exibirTrecho($idtrecho);
        $statusTrecho = $dbTableGrupo->statusTrecho($idtrecho);
        $codigosAceitos = $dbTableGrupo->listarCodigosAceitos($idgrupo, $idtrecho);

        $this->view->listaDosTrechosPorGrupo = $trechos;
        $this->view->listaDosUsuariosPorGrupo = $usuarios;
        $this->view->listaDosCodigosPorTrecho = $codigos;
        $this->view->exibirTrecho = $trecho;
        $this->view->listaDosCodigosAceitos = $codigosAceitos;
        $this->view->statusTrecho = $statusTrecho;
        $this->view->getGrupoPorId = $idgrupo;
    }

    public function statusAceitoAction()
    {
        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados);die();

            $idcodigo = explode(',', $dados['id']);
//            $status = explode(',', $dados['status']);

            $dbTableCodigo = new Application_Model_DbTable_Codigo();
//            $dbTableCodigo->statusAceito($idcodigo, $status);
            $dbTableCodigo->statusAceito($idcodigo);
        }
    }

    public function sairAction()
    {
        $sessionUsuario = new Application_Model_SessaoUsuario();
//        $usuario = $sessionUsuario->getSessao();

        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();

        $this->_redirect('');
    }

    public function excluirCodigoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getRequest()->getParam('id');
        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $idcodigo = $dbTableCodigo->excluirCodigoUsuario($id);

        if ($idcodigo != null) {
            echo $this->_helper->json(array('flag' => 'ok'));
        } else {
            echo $this->_helper->json(array('flag' => 'nok'));
        }
    }

    public function acessoNegadoAction()
    {
        
    }

}
