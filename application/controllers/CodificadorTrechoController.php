<?php

class CodificadorTrechoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function cadastrarAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view->layout()->disableLayout();

        if ($this->getRequest()->isPost()) {
            $id = $this->getRequest()->getParam('id');
            $dados = explode(',', $id);

            $dbTableUsuario = new Application_Model_DbTable_Usuario();
            $dbTableCodificadorTrecho = new Application_Model_DbTable_CodificadorTrecho();
            $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
            
            $consult = $dbTableCodificadorTrecho->consultarCodificadorTrecho($dados[0], $dados[1]);
            $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
            $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);

            if (empty($consult)) {
                $result = $dbTableCodificadorTrecho->cadastrar($dados[0], $dados[1]);
            } else {
                $result = $dbTableCodificadorTrecho->adicionar($dados[0], $dados[1]);
            }
            
            $dbTableUsuario->sendEmailAdicionado($dados, $avaliador[0]['login']);

            if ($result !== NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }
    }

    public function removerAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view->layout()->disableLayout();

        if ($this->getRequest()->isPost()) {
            $id = $this->getRequest()->getParam('id');
            $dados = explode(',', $id);


            $dbTableCodificadorTrecho = new Application_Model_DbTable_CodificadorTrecho();
            $dbTableCodificadorTrecho->remover($dados[0], $dados[1]);

            echo $this->_helper->json(array('flag' => 'ok'));
        }
    }

}
