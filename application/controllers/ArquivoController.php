<?php

class ArquivoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/arquivo_avaliador.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script_codificador.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script_arquivos.js'));

        $idprojeto = $this->getRequest()->getParam('id');

        $dbTableProjeto = new Application_Model_DbTable_Projeto();
        $dbTableTrecho = new Application_Model_DbTable_Trecho();
        $dbTableCodificadorTrecho = new Application_Model_DbTable_CodificadorTrecho();

        $trechos = $dbTableTrecho->getTrechosPorIdProjeto($idprojeto);
        $projeto = $dbTableProjeto->getProjetoPorIdProj($idprojeto);
        $codificadores = $dbTableCodificadorTrecho->getCodificadoresPorProjeto($idprojeto);
//        var_dump($codificadores); die();

        $this->view->codificadores = $codificadores;
        $this->view->listaArquivos = $trechos;
        $this->view->projeto = $projeto;
    }

    public function continuarAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/arquivo_avaliador.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script_codificador.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script_arquivos.js'));

        $idprojeto = $this->getRequest()->getParam('id');

        $dbTableProjeto = new Application_Model_DbTable_Projeto();
        $dbTableTrecho = new Application_Model_DbTable_Trecho();
        $dbTableCodificadorTrecho = new Application_Model_DbTable_CodificadorTrecho();

        $trechos = $dbTableTrecho->getTrechosPorIdProjeto($idprojeto);
        $projeto = $dbTableProjeto->getProjetoPorIdProj($idprojeto);
        $codificadores = $dbTableCodificadorTrecho->getCodificadoresPorProjeto($idprojeto);

        $this->view->codificadores = $codificadores;
        $this->view->listaArquivos = $trechos;
        $this->view->projeto = $projeto;
    }

}
