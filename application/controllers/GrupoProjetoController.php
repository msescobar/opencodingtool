<?php

class GrupoProjetoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/jquery.dataTables.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script-dataTables.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));

        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);

        $dbTableProjeto = new Application_Model_DbTable_Projeto();
        $projetos = $dbTableProjeto->getTodosProjetosPorIdAvaliador($avaliador[0]['idavaliador']);
        $this->view->listaDosProjetos = $projetos;
        
        $dbTableGrupoProjeto = new Application_Model_DbTable_Grupoprojeto();
        $grupos = $dbTableGrupoProjeto->listarGrupoProjetoPorIdAvaliador($avaliador[0]['idavaliador']);
//        var_dump($grupos);die();
        $this->view->listaDosGruposProjetos = $grupos;
    }

    public function cadastrarAction()
    {
        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados);die();

            if ($dados['idprojeto'] == '' || $dados['idprojeto'] == 'Escolha um Projeto...') {

                echo $this->_helper->json(array('flag' => 'nok'));
            } else {

                $dbTableGrupoprojeto = new Application_Model_DbTable_Grupoprojeto();
                $idgrupo = $dbTableGrupoprojeto->cadastrar($dados);

                if ($idgrupo != NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        }
    }

    public function editarAction()
    {
        $id = $this->getRequest()->getParam('id');
//        var_dump($id);die();
        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $usuario = $dbTableUsuario->getUsuarioPorId($id);

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
            $result = $dbTableUsuario->editar($id, $dados);

            $dbTableCodificador = new Application_Model_DbTable_Codificador();
            $codificador = $dbTableCodificador->getCodificadorPorIdUsuario($id);
            $dbTableCodificador->editar($codificador['idcodificador'], $dados);

            if ($result != NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }

        $this->view->usuario = $usuario;
    }

    public function excluirAction()
    {
        $id = $this->getRequest()->getParam('id');
        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $idUsuario = $dbTableUsuario->excluir($id);

        if ($idUsuario != null) {
            $this->view->message = "Cadastro excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

}
