<?php

class CodigoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_discente');

        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/codigo.js'));

//        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
//        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
//        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
//
//        $dbTableGrupoCodigo = new Application_Model_DbTable_Grupocodigo();
//        $grupos = $dbTableGrupoCodigo->getGrupoCodigosPorIdAvaliadorIdProjeto($avaliador[0]['idavaliador']);
//        $this->view->listaDosGruposCodigos = $grupos;
//        $dbTableTrecho = new Application_Model_DbTable_Trecho();
//        $trecho = $dbTableTrecho->listarTodosTrechosPorGrupo($idusuario);
//        $this->view->listaDosTrechos = $trecho;

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $lista = $dbTableCodigo->listarTodosCodigos();
        $this->view->listaDosCodigos = $lista;
    }

    public function indexAction()
    {
        $this->_helper->layout->setLayout('layout_trechos');
    }

    public function listarAction()
    {
        $this->_helper->layout->setLayout('layout_trechos');
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/codigo.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/listar.js'));

        $idtrecho = $this->getRequest()->getParam('id');
        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $dbTableGrupoProjeto = new Application_Model_DbTable_Grupoprojeto();
        $dbTableGrupoCodigo = new Application_Model_DbTable_GrupoCodigo();
        $dbTableTrecho = new Application_Model_DbTable_Trecho();

        $grupoprojeto = $dbTableGrupoProjeto->getIdProjetoPorIdTrecho($idtrecho);
        $gruposcodigos = $dbTableGrupoCodigo->getGrupoCodigosPorIdProjeto($grupoprojeto[0]['idprojeto']);

        $codigos = $dbTableCodigo->listarCodigosPorTrecho($idtrecho, $idusuario);
//        var_dump($codigos['GRUPO']);die();
        $trecho = $dbTableCodigo->exibirTrecho($idtrecho);

        $dbTableCodificador = new Application_Model_DbTable_Codificador();
        $codificador = $dbTableCodificador->getCodificadorPorIdUsuario($idusuario);
        $trechos = $dbTableTrecho->getTrechoPorIdProjetoIdCodificador($grupoprojeto[0]['idprojeto'], $codificador['idcodificador']);

        $this->view->listaDosTrechos = $trechos;
        $this->view->listaDosGruposCodigos = $gruposcodigos;
        $this->view->listaDosCodigosPorTrecho = $codigos;
        $this->view->exibirTrecho = $trecho;
    }

    public function cadastrarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->view->message = '';

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados); die();
            $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
            $dbTableCodigo = new Application_Model_DbTable_Codigo();
            $id = $dbTableCodigo->cadastrar($dados, $idusuario);

            if ($id != NULL) {
                $this->view->message = "Cadastro realizada com sucesso!";
            } else {
                $this->view->message = "Não foi possível efetuar o cadastro!";
            }
        }
    }

    public function cadastrarCodigoAceitoAction()
    {
        $this->view->message = '';

        if ($this->getRequest()->isPost()) {

            $dados = $this->getRequest()->getParams();
//            var_dump($dados);die();
            $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
            $dbTableCodigo = new Application_Model_DbTable_Codigo();
            $id = $dbTableCodigo->cadastrarCodigoAceito($dados, $idusuario);

            if ($id != NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }
    }

    public function aceitoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getRequest()->getParam('id');

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $idcodigo = $dbTableCodigo->aceitarCodigo($id);

        if ($idcodigo != null) {
            $this->view->message = "Código excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

    public function excluirCodigoAvaliadorAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getRequest()->getParam('id');
//        var_dump($id); die();

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $idcodigo = $dbTableCodigo->excluirAvaliador($id);

        if ($idcodigo != null) {
            $this->view->message = "Código excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

    public function excluirCodigoAction()
    {
        $id = $this->getRequest()->getParam('id');
        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $idcodigo = $dbTableCodigo->excluirCodigoUsuario($id);

        if ($idcodigo != null) {
            echo $this->_helper->json(array('flag' => 'ok'));
        } else {
            echo $this->_helper->json(array('flag' => 'nok'));
        }
    }

    public function editarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados); die();
            $dbTableCodigo = new Application_Model_DbTable_Codigo();
            $result = $dbTableCodigo->editarCodigo($dados);

            if ($result !== NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }

//        $this->view->codigo = $codigo;
    }

    public function editarStatusAction()
    {
        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();

            $idcodigo = explode(',', $dados['idcodigo']);
            $status = explode(',', $dados['status']);

            $dbTableAvaliacao = new Application_Model_DbTable_Avaliacao();
            $dbTableAvaliacao->statusConcluido($dados['idavaliacao']);

            for ($i = 0; $i < sizeof($idcodigo); $i++) {
                $dbTableCodigo = new Application_Model_DbTable_Codigo();
                $dbTableCodigo->editarStatus($idcodigo[$i], $status[$i]);
            }
        }
    }

}
