<?php

class CriarContaController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_inicial');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/bootbox.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/criar-conta.js'));
    }

    public function cadastrarAction()
    {
        try {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableUsuario = new Application_Model_DbTable_Usuario();
                $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
//                $dbTableCodificador = new Application_Model_DbTable_Codificador();
                $validator = new Zend_Validate_EmailAddress();

                if ($validator->isValid($dados['login'])) {
                    //Verifica exitencia de Codificador
                    $resultCodificador = $dbTableUsuario->getCodificadorPorLoginEmUso($dados['login']);

                    //O usuário possui conta de Codificador? não
                    if (empty($resultCodificador)) {

                        //Verifica exitencia de Avaliador
                        $resultAvaliador = $dbTableUsuario->getUsuarioPorLoginEmUso($dados['login']);

                        //O usuário possui conta de Avaliador? não
                        if (empty($resultAvaliador)) {
                            $idusuario = $dbTableUsuario->cadastrarAvaliador($dados);
                            $id = $dbTableAvaliador->cadastrar($dados, $idusuario);

                            if ($id !== NULL) {
                                echo $this->_helper->json(array('flag' => 'ok'));
                            } else {
                                echo $this->_helper->json(array('flag' => 'nok'));
                            }
                        } else {
                            if ($resultAvaliador[0]['idperfil'] == 4) {
                                echo $this->_helper->json(array('flag' => 'existe'));
                            } else {
                                echo $this->_helper->json(array('flag' => 'avaliador'));
                            }
                        }
                    } else {
//                      //Já possuia conta de Codificador, sim
                        //Verifica exitencia de conta de Avaliador
                        $resultAval = $dbTableUsuario->getUsuarioPorLoginEmUso($dados['login']);
                        
                        //O usuário possui conta de Avaliador? não
                        if (empty($resultAval)) {
                            
                            if ($resultCodificador[0]['idperfil'] == 3) {
                                $resultUsuario = $dbTableUsuario->getUsuarioPorLogin($dados['login']);
                                
                                $dbTableAvaliador->cadastrar($dados, $resultUsuario['idusuario']);
                                $dbTableUsuario->editarPerfilParaAmbos(intval($resultUsuario['idusuario']));

                                echo $this->_helper->json(array('flag' => 'alterado'));
                                
                            } elseif ($resultCodificador[0]['idperfil'] == 4) {
                                
                                echo $this->_helper->json(array('flag' => 'existe'));
                            }
                        } else {
                            if ($resultAval[0]['idperfil'] == 4) {
                                echo $this->_helper->json(array('flag' => 'existe'));
                            } else {
                                echo $this->_helper->json(array('flag' => 'avaliador'));
                            }
                        }
                    }
                }
            } else {
                echo $this->_helper->json(array('flag' => 'email'));
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

}
