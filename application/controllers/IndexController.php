<?php

class IndexController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_inicial');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/bootbox.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script_index.js'));
        
        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();
        $login = new Application_Form_Login();

        if ($this->getRequest()->isPost()) {
            if ($login->isValid($_POST)) {
                $dados = $login->getValues();

                $db = Zend_Db_Table::getDefaultAdapter();
                $authAdapter = new Zend_Auth_Adapter_DbTable(
                        $db, 'usuario', 'login', 'senha', 'idperfil');

                $authAdapter->setIdentity($dados['login']);
                $authAdapter->setCredential(base64_encode($dados['senha']));

                $result = $authAdapter->authenticate();

                if ($result->isValid()) {

                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject(array('idusuario', 'login', 'senha', 'idperfil')));
                } else {

                    $this->view->error = "Usuário ou senha incorreta!";
                }
            }
        }

        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity()) {
            $modelUsuario = new Application_Model_DbTable_Usuario();
            $modelSessaoUsuario = new Application_Model_SessaoUsuario();

            $usuario = $modelUsuario->getUsuarioPorLogin($dados['login']);
            $modelSessaoUsuario->inserirDados($usuario);
//
            if ($usuario->getIdperfil() == 2) {
                $this->_redirect('/admin-avaliador');
                
            } elseif ($usuario->getIdperfil() == 3) {
                $this->_redirect('/admin-codificador');
                
            } elseif ($usuario->getIdperfil() == 4) {
                $this->_redirect('/selecionar');
            }
        }

        $this->view->login = $login;
    }

    public function sairAction()
    {
        $sessionUsuario = new Application_Model_SessaoUsuario();
        $usuario = $sessionUsuario->getSessao();

        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();

        $this->_redirect('');
    }

}
