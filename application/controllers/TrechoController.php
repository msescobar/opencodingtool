<?php

class TrechoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
//        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $idprojeto = $this->getRequest()->getParam('id');

        $dbTableTrecho = new Application_Model_DbTable_Trecho();
        $dbTableProjeto = new Application_Model_DbTable_Projeto();

        $projeto = $dbTableProjeto->getProjetoPorId($idprojeto);
        $arquivos = $dbTableTrecho->getTrechosPorIdProjeto($idprojeto);
//        var_dump($projeto['titulo']);die();

        $this->view->projeto = $projeto;
        $this->view->listaDosArquivos = $arquivos;
    }

    public function avaliarAction()
    {
//        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $idprojeto = $this->getRequest()->getParam('id');

        $dbTableTrecho = new Application_Model_DbTable_Trecho();
        $dbTableProjeto = new Application_Model_DbTable_Projeto();

        $projeto = $dbTableProjeto->getProjetoPorId($idprojeto);
        $arquivos = $dbTableTrecho->getTrechosPorIdProjeto($idprojeto);
//        var_dump($projeto['titulo']);die();

        $this->view->projeto = $projeto;
        $this->view->listaDosArquivos = $arquivos;
    }

    public function codificadorAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/codigo.js'));
        $idprojeto = $this->getRequest()->getParam('id');

        $dbTableTrecho = new Application_Model_DbTable_Trecho();
        $dbTableProjeto = new Application_Model_DbTable_Projeto();

        $projeto = $dbTableProjeto->getProjetoPorId($idprojeto);
//        var_dump($projeto); die();
        $arquivos = $dbTableTrecho->getTrechosPorIdProjeto($idprojeto);
//        var_dump($arquivos);die();

        $this->view->projeto = $projeto;
        $this->view->listaDosArquivos = $arquivos;
    }

//    public function testeAction()
//    {
//        $this->_helper->layout()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
//
//        if ($this->getRequest()->isPost()) {
//            $dados = $this->getRequest()->getParams();
//
//            $upload = new Zend_File_Transfer();
//            $arquivo = $upload->getFileInfo();
//
//            if ($arquivo['arquivo']['type'] == 'application/pdf') {
//                $dbTableTrecho = new Application_Model_DbTable_Trecho();
//
//                $idtrecho = $dbTableTrecho->cadastrarTrecho($arquivo["arquivo"]["name"], $dados['idprojetoarquivo'], 1);
//                
//                $modelImagemVideo = new Application_Model_ImagemVideo();
//
//                $modelImagemVideo->setCaminhoTemp($arquivo["arquivo"]["tmp_name"]);
//                $modelImagemVideo->setIdentidade(Zend_Auth::getInstance()->getIdentity());
//                $caminho = $modelImagemVideo->moverArquivoCadastrado($arquivo, 'pdf');
//                $result = $dbTableTrecho->alterarCaminhoFoto($caminho, $idtrecho);
//
//                if ($result !== NULL) {
//                    echo $this->_helper->json(array('flag' => 'ok'));
//                } else {
//                    echo $this->_helper->json(array('flag' => 'nok'));
//                }
//            }
//        }
//    }

    public function importarArquivoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                if (isset($_FILES['arquivo'])) {
                    $nome = $_FILES["arquivo"]["name"];
                    $validarExtensao = explode(".", $nome);
                    $extensao = end($validarExtensao);

//                    if ($extensao !== "csv" && $extensao !== "txt") {
                        if ($extensao === "pdf" || $extensao === "docx" || $extensao === 'doc') {
//                            var_dump($extensao); die();

//                        echo "Extensão inválida!";
                            $dbTableTrecho = new Application_Model_DbTable_Trecho();

                            $upload = new Zend_File_Transfer();
                            $arquivo = $upload->getFileInfo();

                            $idtrecho = $dbTableTrecho->cadastrarTrecho($arquivo["arquivo"]["name"], $dados['idprojetoarquivo'], 1);

                            $arquivo['id'] = $idtrecho;

                            $modelImagemVideo = new Application_Model_ImagemVideo();

                            $modelImagemVideo->setCaminhoTemp($arquivo["arquivo"]["tmp_name"]);
                            $modelImagemVideo->setIdentidade(Zend_Auth::getInstance()->getIdentity());
                            $caminho = $modelImagemVideo->moverArquivoCadastrado($arquivo, $extensao);
                            $result = $dbTableTrecho->alterarCaminhoFoto($caminho, $idtrecho);

                            if ($result !== NULL) {
                                echo $this->_helper->json(array('flag' => 'ok'));
                            } else {
                                echo $this->_helper->json(array('flag' => 'nok'));
                            }
                        } else {
                            
                        }
//                    }
                }
            }
        } catch (Zend_Controller_Exception $e) {
            echo "Exceção capturada: " . get_class($e) . "\n";
            echo "Mensagem: " . $e->getMessage() . "\n";
            // Other code to recover from the error
        }
    }

    public function removerAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableTrecho = new Application_Model_DbTable_Trecho();
                $result = $dbTableTrecho->remover($dados['idtrecho']);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

}
