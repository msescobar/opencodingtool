<?php

class AdministradorController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/administrador.js'));

        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $listaDeAvaliadores = $dbTableUsuario->getAvaliadores(2);

        $this->view->listaDosAvaliadores = $listaDeAvaliadores;
    }

    public function cadastrarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();

            $dbTableUsuario = new Application_Model_DbTable_Usuario();
            $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

            $idusuario = $dbTableUsuario->cadastrarAvaliador($dados);
            $avaliador = $dbTableAvaliador->cadastrar($dados, intval($idusuario));

            if ($avaliador !== NULL) {

                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }
    }

    public function editarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();

            $dbTableUsuario = new Application_Model_DbTable_Usuario();
            $idusuario = $dbTableUsuario->editar(intval($dados['id']), $dados);

            if ($idusuario != NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }
    }

    public function excluirAction()
    {
        $id = $this->getRequest()->getParam('id');
        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $idUsuario = $dbTableUsuario->excluir(intval($id));

        if ($idUsuario != NULL) {
            echo $this->_helper->json(array('flag' => 'ok'));
        } else {
            echo $this->_helper->json(array('flag' => 'nok'));
        }
    }

}
