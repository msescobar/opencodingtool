<?php

class GrupoCodigoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/jquery.dataTables.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script-dataTables.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));

        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);

        $dbTableProjeto = new Application_Model_DbTable_Projeto();
        $projetos = $dbTableProjeto->getTodosProjetosPorIdAvaliador($avaliador[0]['idavaliador']);
        $this->view->listaDosProjetos = $projetos;

        $dbTableGrupoCodigo = new Application_Model_DbTable_Grupocodigo();
        $grupos = $dbTableGrupoCodigo->getGrupoCodigosPorIdAvaliadorIdProjeto($avaliador[0]['idavaliador']);

        $this->view->listaDosGruposCodigos = $grupos;
    }

    public function cadastrarAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view->layout()->disableLayout();

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados);die();
//            if ($dados['idtrecho'] == '' || $dados['grupo'] == 'Escolha um Projeto...') {
//                echo $this->_helper->json(array('flag' => 'nok'));
//            } else {

            $dbTableGrupocodigo = new Application_Model_DbTable_Grupocodigo();
            $idgrupo = $dbTableGrupocodigo->cadastrar($dados);

            if ($idgrupo != NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
//            }
        }
    }

    public function editarAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view->layout()->disableLayout();

        $id = $this->getRequest()->getParam('id');
        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $usuario = $dbTableUsuario->getUsuarioPorId($id);

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();
//            var_dump($dados); die();

            $dbTableGrupoCodigo = new Application_Model_DbTable_Grupocodigo();
            $result = $dbTableGrupoCodigo->editar($dados['id'], $dados);

            if ($result != NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }

        $this->view->usuario = $usuario;
    }

    public function excluirAction()
    {
        $id = $this->getRequest()->getParam('id');
        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $idUsuario = $dbTableUsuario->excluir($id);

        if ($idUsuario != null) {
            $this->view->message = "Cadastro excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

}
