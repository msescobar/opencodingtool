<?php

class DiscenteController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_discente');
    }

    public function indexAction()
    {
        
    }

    public function sairAction()
    {
        $sessionUsuario = new Application_Model_SessaoUsuario();
        $usuario = $sessionUsuario->getSessao();

        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();

        $this->_redirect('');
    }

    public function acessoNegadoAction()
    {
        
    }

}