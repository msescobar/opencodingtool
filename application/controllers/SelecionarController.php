<?php

class SelecionarController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_inicial');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/bootbox.min.js'));
    }

    public function sairAction()
    {
        $sessionUsuario = new Application_Model_SessaoUsuario();
        $usuario = $sessionUsuario->getSessao();

        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();

        $this->_redirect('');
    }

}
