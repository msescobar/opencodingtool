<?php

class UsuarioController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/jquery.dataTables.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script-dataTables.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));

        $dbTableCodificador = new Application_Model_DbTable_Codificador();
        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;

        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
        $lista = $dbTableCodificador->listarTodosCodificadoresPorAvaliador($avaliador[0]['idavaliador']);

        $this->view->listaDosUsuarios = $lista;
    }

    public function cadastrarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();

            $dbTableUsuario = new Application_Model_DbTable_Usuario();
            $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
            $dbTableCodificador = new Application_Model_DbTable_Codificador();
            $validator = new Zend_Validate_EmailAddress();

            $idusuarioAvaliador = Zend_Auth::getInstance()->getIdentity()->idusuario;
            $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuarioAvaliador);

            if ($validator->isValid($dados['login'])) {
                $resultAvaliador = $dbTableUsuario->getUsuarioPorLoginEmUso($dados['login']);
                
                //O usuário possui conta de Avaliador, sim
                if (!empty($resultAvaliador[0])) {
                    if ($resultAvaliador[0]['idperfil'] == 2) {
                        $dbTableUsuario->editarPerfilParaAmbos(intval($resultAvaliador[0]['idusuario']));
                    }
                }

                //Verifica se o usuário participa do projeto deste Avaliador
                $resultCodificador = $dbTableUsuario->getUsuarioPorLoginComAvaliador($dados['login'], intval($avaliador[0]['idavaliador']));
                
                //O Codificador não participa do projeto deste avaliador
                if (empty($resultCodificador[0])) {
                    //Verifica se existe conta de usuário cadastrada no sistema
                    $resultUsuario = $dbTableUsuario->getUsuarioPorLogin($dados['login']);
                    
                    if (empty($resultUsuario)) {
                        
                        $idusuario = $dbTableUsuario->cadastrar($dados);
                        $idcodificador = $dbTableCodificador->cadastrar(intval($idusuario), intval($avaliador[0]['idavaliador']));
                    
                        $dbTableUsuario->sendEmail($dados, $dados['senha'], $avaliador[0]['login']);
                        
                    } else {
                        $idcodificador = $dbTableCodificador->cadastrar(intval($resultUsuario['idusuario']), intval($avaliador[0]['idavaliador']));
                    
                        $dbTableUsuario->sendEmail($dados, base64_decode($resultUsuario['senha']), $avaliador[0]['login']);
                    }

                    if ($idcodificador !== NULL) {
//                
                        if (empty($resultAvaliador[0])) {
                            echo $this->_helper->json(array('flag' => 'ok'));
                            
                        } else if ($resultAvaliador[0]['idperfil'] == 2) {
                            echo $this->_helper->json(array('flag' => 'alterado'));
                            
                        } elseif ($resultAvaliador[0]['idperfil'] == 4) {
//                            echo $this->_helper->json(array('flag' => 'existe'));   
                        }
                    }
                } else {
//                    echo $this->_helper->json(array('flag' => 'participa'));
                }
            } else {
                echo $this->_helper->json(array('flag' => 'email'));
            }
        }
    }

    public function editarAction()
    {
        $id = $this->getRequest()->getParam('id');
        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $usuario = $dbTableUsuario->getUsuarioPorId($id);

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();

            $idusuario = $dbTableUsuario->editar($id, $dados);
//            $dbTableCodificador = new Application_Model_DbTable_Codificador();
//            $codificador = $dbTableCodificador->getCodificadorPorIdUsuario($id);
//            $idcodificador = $dbTableCodificador->editar($codificador['idcodificador'], $dados);


            if ($idusuario != NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }

        $this->view->usuario = $usuario;
    }

    public function excluirAction()
    {
        $id = $this->getRequest()->getParam('id');
        $dbTableUsuario = new Application_Model_DbTable_Usuario();
        $idUsuario = $dbTableUsuario->excluir($id);

        if ($idUsuario != null) {
            $this->view->message = "Cadastro excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

    public function novaSenhaAction()
    {//FIRST
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableUsuario = new Application_Model_DbTable_Usuario();

                $result = $dbTableUsuario->getUsuarioEsquecidoPorEmail($dados['email']);
                $dbTableUsuario->setRedefinirSenha(intval($result[0]['idusuario']));
//                var_dump(intval($result[0]['idusuario'])); die();
                if (!empty($result)) {
                    $dbTableUsuario->renovarSenha($dados['email'], intval($result[0]['idusuario']));

                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function redefinirsenhaAction()
    {
        $this->_helper->layout->setLayout('layout_inicial');
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/bootbox.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/script_index.js'));

        try {
            $idusuario = $this->getRequest()->getParam('hash');

            $dbTableUsuario = new Application_Model_DbTable_Usuario();
            $status = $dbTableUsuario->getStatusRedefinirPorId(intval(base64_decode(substr($idusuario, -4))));
//            var_dump($status[0]['redefinir']); die();

            if (intval($status[0]['redefinir']) == 0) {
                header("Location: http://opencodingtool.com.br/public/");
                exit;
            } else if (intval($status[0]['redefinir']) == 1) {
                $this->view->idusuario = intval(base64_decode(substr($idusuario, -4)));
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function alterarSenhaAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                if ($dados['senha'] == $dados['repita-nova-senha'] && $dados['senha'] !== '') {
                    $dbTableUsuario = new Application_Model_DbTable_Usuario();

                    $usuario = $dbTableUsuario->getUsuarioPorId($dados['idusuario']);
                    $resultUsuario = $dbTableUsuario->renovar($dados, intval($usuario['idusuario']));
                    $dbTableUsuario->setSenhaAlterada(intval($dados['idusuario']));

                    if ($resultUsuario !== NULL) {
                        echo $this->_helper->json(array('flag' => 'ok'));
                    } else {
                        echo $this->_helper->json(array('flag' => 'error'));
                    }
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

}
