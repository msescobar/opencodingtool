<?php

class AvaliacaoController extends Zend_Controller_Action {

    public function init()
    {
        
    }

    public function editarStatusAction()
    {
        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getParams();

//            $dbTableAvaliacao = new Application_Model_DbTable_Avaliacao();
//            $dbTableAvaliacao->statusConcluido($idavaliacao);

            $idcodigo = explode(',', $dados['idcodigo']);
            $status = explode(',', $dados['status']);

            for ($i = 0; $i < sizeof($idcodigo); $i++) {
                $dbTableCodigo = new Application_Model_DbTable_Codigo();
                $dbTableTrecho = new Application_Model_DbTable_Trecho();
                $dbTableTrecho->statusFinalizado($dados['idtrecho']);
                $dbTableCodigo->statusNaoAceito($idcodigo[$i], $status[$i]);
            }
        }
    }

    public function verificarAction()
    {
        $this->_helper->layout->setLayout('layout_admin');
        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
        $dbTableAvaliacao = new Application_Model_DbTable_Avaliacao();

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
        $lista = $dbTableAvaliacao->listarAvaliacao($avaliador[0]['idavaliador']);

        $this->view->listaDasAvaliacoes = $lista;
    }

    public function relatorioCodigosAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/csv.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/FileSaver.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/tableExport.min.js'));

        $this->_helper->layout->setLayout('layout_admin');

        $dbTableAvaliacao = new Application_Model_DbTable_Avaliacao();
        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
        $listar = $dbTableAvaliacao->listarRelatorioCodigosAvaliacao($avaliador[0]['idavaliador']);

        $this->view->listaDasAvaliacoes = $listar;
    }

    public function downloadCsvAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view->layout()->disableLayout();

        header("Content-Type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=avaliacoes.csv");

        $output = fopen('php://output', 'w');

        $cells = array('Login', 'Código', 'Trecho', 'Status');

        fputcsv($output, $cells);

        $link = mysqli_connect('localhost', 'root', '', 'legacysystemreport');

        $query = 'SELECT usuario.login, codigo.descricao, codigo.idtrecho, codigo.status"
                . " FROM codigo, usuario WHERE codigo.idusuario = usuario.idusuario';

        $result = mysqli_query($link, $query);
//        var_dump($result);die();

        while ($row = mysqli_fetch_assoc($result)) {
            fputcsv($output, $row);
        }

        fclose($output);
    }

}
