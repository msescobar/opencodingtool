<?php

class ProjetoController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function cadastrarAction()
    {
        try {
            $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/projeto.js'));

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

                $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;

                $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
                $idprojeto = $dbTableProjeto->cadastrar($dados, $avaliador[0]['idavaliador']);

                if ($idprojeto !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok', 'idprojeto' => $idprojeto));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function editarAction()
    {
        try {
            $this->view->message = "";

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();
                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $result = $dbTableProjeto->editar($dados);

                if ($result != NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function editarStatusAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $result = $dbTableProjeto->statusConcluido($dados['idprojeto']);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function iniciarAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();
//                var_dump($dados); die(); //idprojeto

                $dbTableTrecho = new Application_Model_DbTable_Trecho();
                $dbTableCodificadorTrecho = new Application_Model_DbTable_CodificadorTrecho();
                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

                $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
                $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
//                var_dump($avaliador[0]['login']); die();

                $trechos = $dbTableTrecho->getTrechoPorIdProjeto(intval($dados['idprojeto']));
//                var_dump($trechos); die();
//                var_dump(sizeof($trechos)); die();
                
                for ($i = 0; $i < sizeof($trechos); $i++){
//                    var_dump($trechos[$i]['idtrecho']); die();
                    $codificadores = $dbTableCodificadorTrecho->getCodificadorPorIdTrecho(intval($trechos[$i]['idtrecho']));
//                    var_dump($codificadores); die();
                    
                    for ($k = 0; $k < sizeof($codificadores); $k++){
//                        var_dump($trechos[$i]); die();
//                        var_dump($codificadores); die();
//                        var_dump($codificadores[$k]);
                        //Enviar Email
                        $dbTableProjeto->sendEmail($codificadores[$k], $avaliador[0]['login']);
//                        var_dump('freio de mão!'); die();
                    }
                    
                }
                
//                var_dump('freio de mão!'); die();
                
                $result = $dbTableProjeto->iniciar(intval($dados['idprojeto']));

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function pausarAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $result = $dbTableProjeto->pausar($dados['idprojeto']);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function finalizarAvaliacaoAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $result = $dbTableProjeto->finalizarAvaliacao($dados['idprojeto']);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function finalizarCategorizacaoAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $resultCodigos = $dbTableProjeto->getCodigosVinculadosAoProjeto(intval($dados['idprojeto']));
                $resultCategorias = $dbTableProjeto->getCategoriasVinculadasAoProjeto(intval($dados['idprojeto']));

                $resultado = '';
                for ($i = 0; $i < sizeof($resultCodigos); $i++) {
                    if ($resultCodigos[$i]['status'] == '1') {
                        $resultado = 'nok';
                    }
                }

                if ($resultado != 'nok') {
                    for ($i = 0; $i < sizeof($resultCategorias); $i++) {
                        $resultVinculos = $dbTableProjeto->getVerificarCategoriasVazias($resultCategorias[$i]['idgrupocodigo']);

                        if (empty($resultVinculos)) {
                            $resultado = 'nok';
                        }
                    }
                }

                if ($resultado == '') {
                    $dbTableProjeto->finalizarCategorizacao(intval($dados['idprojeto']));
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function removerAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $result = $dbTableProjeto->remover($dados['idprojeto']);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function visualizarAction()
    {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->layout()->disableLayout();

            $idprojeto = $this->getRequest()->getParam('id');
//            var_dump($idprojeto); die();

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();

                $dbTableProjeto = new Application_Model_DbTable_Projeto();
                $result = $dbTableProjeto->remover($dados['idprojeto']);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function exportarAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNeverRender(true);

        try {


            $dbTableProjeto = new Application_Model_DbTable_Projeto();
            $idprojeto = $this->getRequest()->getParam('id');
            $projeto = $dbTableProjeto->getProjetoPorId($idprojeto);

            header('Content-type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=' . $projeto['titulo'] . '.csv');

            $codigos = $dbTableProjeto->getCodigosCategorizadosPorIdPj($idprojeto);
//            var_dump($codigos); die();
            $fp = fopen('php://output', 'w');

            $array = [
                "projeto" => "projeto",
                "código" => "código",
                "arquivo" => "arquivo",
                "categoria" => "categoria"
            ];

            fputcsv($fp, $array, ',', ';');
            for ($i = 0; $i < sizeof($codigos); $i++) {
//                var_dump($codigos[$i]); die();
//                fputcsv($fp, ['projeto', 'codigo', 'categoria', 'arquivo'], ',', ';');
                fputcsv($fp, $codigos[$i], ',', ' ');
            }

            fclose($fp);

//            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function relatorioAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $modelRelatorio = new Application_Model_Relatorio();
            $dbTableProjeto = new Application_Model_DbTable_Projeto();
            $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

            $idprojeto = $this->getRequest()->getParam('id');

            $avaliador = $dbTableAvaliador->getAvaliadorPorIdProjeto($idprojeto);
            $projeto = $dbTableProjeto->getProjetoPorId($idprojeto);
            $codigos = $dbTableProjeto->getCodigosCategorizadosPorIdProjeto($idprojeto);

            $html = $modelRelatorio->criarRelatorio($this->view->baseUrl(''), $projeto['titulo'], $avaliador[0]['login'], $codigos);

//            var_dump($html); die();

            require_once 'dompdf/autoload.inc.php';
            $dompdf = new Dompdf\Dompdf();
            $dompdf->set_paper('A3');
            $dompdf->set_option('isRemoteEnabled', TRUE);
            $dompdf->load_html($html);
            $dompdf->render();
//            $pdf = $dompdf->output();
            date_default_timezone_set('America/Bahia');
            $filename = "codigos" . date('Y-m-d_H_i_s') . ".pdf";
//            file_put_contents($filename, $pdf);
            $dompdf->stream($filename);
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }
    

}
