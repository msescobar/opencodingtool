<?php

class AdminAvaliadorController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_admin');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin-avaliador.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/csv.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/FileSaver.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/tableExport.min.js'));

        $dbTableProjeto = new Application_Model_DbTable_Projeto();
        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
        $projetos = $dbTableProjeto->getTodosProjetosPorIdAvaliador($avaliador[0]['idavaliador']);
                
        $this->view->listaProjetos = $projetos;
    }

    public function sairAction()
    {
//        $sessionUsuario = new Application_Model_SessaoUsuario();
//        $usuario = $sessionUsuario->getSessao();

        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();

        $this->_redirect('');
    }

    public function acessoNegadoAction()
    {
        
    }

}
