<?php

class AdminController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('layout_avaliacao');
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $idtrecho = $this->getRequest()->getParam('id');

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $trecho = $dbTableCodigo->getTrechoPorIdArquivo($idtrecho);
//        var_dump($idtrecho); die();
        $codificadores = $dbTableCodigo->getCodificadorPorArquivoListando($idtrecho);
//        var_dump($codificadores); die();
//        $listagrupos = $dbTableGrupoCodigo->getGrupoCodigosPorIdAvaliador($avaliador[0]['idavaliador']);
//        $this->view->listaDosGruposCodigos = $grupos;
//        $this->view->listaGrupos = $grupos;
        $this->view->arquivo = $trecho;
        $this->view->listaDosCodificadores = $codificadores;
    }

    public function avaliarAction()
    {
        try {
            $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
            $idtrecho = $this->getRequest()->getParam('id');
//            var_dump($idtrecho); die();

            $dbTableCodigo = new Application_Model_DbTable_Codigo();
//        $dbTableGrupoCodigo = new Application_Model_DbTable_Grupocodigo();
//        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
//        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
//        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
//        $grupos = $dbTableGrupoCodigo->getGrupoCodigosPorIdAvaliador($avaliador[0]['idavaliador'], $idtrecho);
            $trecho = $dbTableCodigo->getTrechoPorIdArquivo($idtrecho);
            $codificadores = $dbTableCodigo->getCodificadorPorArquivo(intval($idtrecho));
//        var_dump($codificadores);die();
//        $listagrupos = $dbTableGrupoCodigo->getGrupoCodigosPorIdAvaliador($avaliador[0]['idavaliador']);
//        $this->view->listaDosGruposCodigos = $grupos;
//        $this->view->listaGrupos = $grupos;
            $this->view->arquivo = $trecho;
            $this->view->listaDosCodificadores = $codificadores;
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function categorizarAction()
    {
        $this->_helper->layout->setLayout('layout_categorizar');

        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $idprojeto = $this->getRequest()->getParam('id');

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $dbTableGrupoCodigo = new Application_Model_DbTable_Grupocodigo();
        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
        $dbTableProjeto = new Application_Model_DbTable_Projeto();

        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;

        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
        $grupos = $dbTableGrupoCodigo->getGrupoCodigosPorIdAvaliadorPorProjeto($avaliador[0]['idavaliador'], $idprojeto);
        $codigos = $dbTableCodigo->getCodigosPorProjeto($idprojeto);
        $projeto = $dbTableProjeto->getProjetoPorId($idprojeto);

        $this->view->projeto = $projeto;
        $this->view->listaDosGruposCodigos = $grupos;
        $this->view->quantidade = count($grupos);
        $this->view->listaGrupos = $grupos;
        $this->view->listaDosCodigosPorGrupo = $codigos;
    }

    public function agruparCodigoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $this->view->message = '';

            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();
//                var_dump($dados); die();
//            $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
                $dbTableGrupocodigohascodigo = new Application_Model_DbTable_Grupocodigohascodigo();

                $id = $dbTableGrupocodigohascodigo->cadastrar($dados);

                if ($id != NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function editarCodigoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            if ($this->getRequest()->isPost()) {
                $dados = $this->getRequest()->getParams();
//                var_dump($dados); die();
                $dbTableCodigo = new Application_Model_DbTable_Codigo();
                $result = $dbTableCodigo->editarCodigo($dados);

                if ($result !== NULL) {
                    echo $this->_helper->json(array('flag' => 'ok'));
                } else {
                    echo $this->_helper->json(array('flag' => 'nok'));
                }
            }
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function cadastrarCodigoAceitoAction()
    {
        $this->view->message = '';

        if ($this->getRequest()->isPost()) {

            $dados = $this->getRequest()->getParams();
//            var_dump($dados);die();
            $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
            $dbTableCodigo = new Application_Model_DbTable_Codigo();
            $id = $dbTableCodigo->cadastrarCodigoAceito($dados, $idusuario);

            if ($id != NULL) {
                echo $this->_helper->json(array('flag' => 'ok'));
            } else {
                echo $this->_helper->json(array('flag' => 'nok'));
            }
        }
    }

    public function aceitoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getRequest()->getParam('id');
        
        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $idcodigo = $dbTableCodigo->aceitarCodigo(intval($id));

        if ($idcodigo != null) {
            $this->view->message = "Código excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

    public function desagruparCodigoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->getRequest()->isPost()) {

            $dados = $this->getRequest()->getParams();

            $dbTableCodigo = new Application_Model_DbTable_Codigo();
            $dbTableGrupocodigohascodigo = new Application_Model_DbTable_Grupocodigohascodigo();
            
            $idGrupoCodigoHasCodigo = $dbTableCodigo->getIdGrupoCodigoHasCodigo(intval($dados['idcodigo']));
            $dbTableGrupocodigohascodigo->excluirCategoria(intval($idGrupoCodigoHasCodigo[0]['id']));
            $idcodigo = $dbTableCodigo->aceitarCodigo(intval($dados['idcodigo']));

            if ($idcodigo != null) {
                $this->view->message = "Código excluido com sucesso!";
            } else {
                $this->view->message = "Não foi possível efetuar a exclusão!";
            }
        }
    }

    public function excluirCodigoAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getRequest()->getParam('id');

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $idcodigo = $dbTableCodigo->excluirCodigo($id);

        if ($idcodigo != null) {
            $this->view->message = "Código excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

    public function excluirCategoriaAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getRequest()->getParam('id');

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $dbTableGrupoCodigo = new Application_Model_DbTable_Grupocodigo();
        $dbTableCategoria = new Application_Model_DbTable_Grupocodigohascodigo();
        $dbTableCategoriaHasCodigo = new Application_Model_DbTable_Grupocodigohascodigo();

        $result = $dbTableCategoriaHasCodigo->getCodigosPorIdCategoria(intval($id));
//        var_dump($result); die();

        if (intval($result) == 0) {

            $dbTableGrupoCodigo->excluir(intval($id));
        } else {

            for ($i = 0; $i < sizeof($result); $i++) {
                //grupocodigohascodigo
                $dbTableCategoria->excluirCategoria(intval($result[$i]['id']));
                $dbTableCodigo->aceitarCodigo(intval($result[$i]['idcodigo']));
//                $dbTableGrupoCodigo->excluir(intval($result[$i]['idgrupocodigo']));
            }
            $dbTableGrupoCodigo->excluir(intval($id));
        }

        echo $this->_helper->json(array('flag' => 'ok'));
    }

    public function excluirCodigoAvaliadorAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = $this->getRequest()->getParam('id');

        $dbTableCodigo = new Application_Model_DbTable_Codigo();
        $idcodigo = $dbTableCodigo->excluirCodigoAvaliador($id);

        if ($idcodigo != null) {
            $this->view->message = "Código excluido com sucesso!";
        } else {
            $this->view->message = "Não foi possível efetuar a exclusão!";
        }
    }

    public function listarArquivosAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $this->_helper->layout->setLayout('layout_avaliacao');

        $idgrupoprojeto = $this->getRequest()->getParam('id');

        $dbTableGrupo = new Application_Model_DbTable_Grupoprojeto();
        $dbTableProjeto = new Application_Model_DbTable_Projeto();
        $projeto = $dbTableProjeto->getProjetoPorIdProjeto($idgrupoprojeto);
        $grupos = $dbTableGrupo->listarGrupoProjetoPorIdProjeto($projeto[0]['idprojeto']);

        $this->view->listaDosGrupos = $grupos;


//        $dbTableAvaliador = new Application_Model_DbTable_Avaliador();
//
//        $idusuario = Zend_Auth::getInstance()->getIdentity()->idusuario;
//
//        $avaliador = $dbTableAvaliador->getAvaliadorPorIdUsuario($idusuario);
        $trechos = $dbTableGrupo->listarTrechosPorIdGrupoProjeto($idgrupoprojeto);

        $this->view->listaDosTrechosPorGrupo = $trechos;
        $this->view->getGrupoPorId = $idgrupoprojeto;
    }

    public function listarcodigosAction()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('dist/js/admin.js'));
        $this->_helper->layout->setLayout('layout_avaliacao');

//        $dbTableGrupo = new Application_Model_DbTable_Grupoprojeto();
//        $grupo = $dbTableGrupo->listarTodosGrupos();
//        $this->view->listaDosGrupos = $grupo;

        $idarquivo = $this->getRequest()->getParam('id');
        var_dump($idarquivo);
        die();
        $dados = explode(',', $ids);
        $idgrupo = $dados['0'];
        $idtrecho = $dados['1'];

//        $trechos = $dbTableGrupo->listarTrechosPorIdGrupo($idgrupo);
//        $usuarios = $dbTableGrupo->listarUsuariosPorGrupo($idgrupo, $idtrecho);
//        $codigos = $dbTableGrupo->listarCodigosPorTrecho($idgrupo, $idtrecho);
//        $trecho = $dbTableGrupo->exibirTrecho($idtrecho);
//        $statusTrecho = $dbTableGrupo->statusTrecho($idtrecho);
//        $codigosAceitos = $dbTableGrupo->listarCodigosAceitos($idgrupo, $idtrecho);
//        $this->view->listaDosTrechosPorGrupo = $trechos;
//        $this->view->listaDosUsuariosPorGrupo = $usuarios;
//        $this->view->listaDosCodigosPorTrecho = $codigos;
//        $this->view->exibirTrecho = $trecho;
//        $this->view->listaDosCodigosAceitos = $codigosAceitos;
//        $this->view->statusTrecho = $statusTrecho;
//        $this->view->getGrupoPorId = $idgrupo;
    }

//    public function excluirCodigoAction()
//    {
//        $this->_helper->layout()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
//
//        $id = $this->getRequest()->getParam('id');
//
//        $dbTableCodigo = new Application_Model_DbTable_Codigo();
//        $idcodigo = $dbTableCodigo->excluir($id);
//
//        if ($idcodigo != null) {
//            $this->view->message = "Código excluido com sucesso!";
//        } else {
//            $this->view->message = "Não foi possível efetuar a exclusão!";
//        }
//    }

    public function sairAction()
    {
//        $sessionUsuario = new Application_Model_SessaoUsuario();
//        $usuario = $sessionUsuario->getSessao();

        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();

        $this->_redirect('');
    }

    public function acessoNegadoAction()
    {
        
    }

}
