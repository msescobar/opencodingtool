<?php

class Application_Form_Login extends Zend_Form {

    public function init()
    {

        $this->setMethod('post');

        $login = new Zend_Form_Element_Text('login');
        $login->setLabel('Email*:')
                ->setAttrib('class', 'form-control')
                ->setAttrib('autofocus', 'autofocus')
                ->setAttrib('autocomplete', 'off')
                ->addValidator(
                        'NotEmpty', //validator name
                        FALSE, //do not break on failure
                        array('messages' => array(
                        'isEmpty' => 'Campo obrigatório'
                    )
                        )
                )
                ->addValidator(new Zend_Validate_StringLength(array('max' => 60)))
                ->setRequired(true);
        $login->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-md-8 col-lg-8')),
            array('label', array('tag' => 'p', 'class' => 'control-label col-md-2 col-lg-2')),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
        ));
        $this->addElement($login);

        $senha = new Zend_Form_Element_Password('senha');
        $senha->setLabel('Senha*:')
                ->setAttrib('class', 'form-control login-margin')
                ->setAttrib('type', 'password')
                ->addValidator(
                        'NotEmpty', //validator name
                        FALSE, //do not break on failure
                        array('messages' => array(
                        'isEmpty' => 'Campo obrigatório'
                    )
                        )
                )
                ->addValidator(new Zend_Validate_StringLength(array('max' => 25)))
                ->setRequired(true);
        $senha->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-md-8 col-lg-8')),
            array('label', array('tag' => 'p', 'class' => 'control-label col-md-2 col-lg-2')),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group input-login-top')),
            array(array('brokea' => 'HtmlTag'), array('tag' => 'br', 'placement' => 'prepend')),
            array(array('brokeaa' => 'HtmlTag'), array('tag' => 'br', 'placement' => 'prepend'))
        ));
        $this->addElement($senha);

        $logar = new Zend_Form_Element_Submit('submit');
        $logar->setLabel('Entrar')
                ->setAttrib('class', 'btn btn-primary btn-largura col-md-12 login-margin-button');
        $logar->setDecorators(array(
            'ViewHelper',
            'Errors',
            'Description',
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group col-md-8 col-lg-8')),
            array(array('left' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-md-2 col-lg-2', 'placement' => 'prepend')),
            array(array('brokem1' => 'HtmlTag'), array('tag' => 'br', 'placement' => 'prepend')),
            array(array('brokem2' => 'HtmlTag'), array('tag' => 'br', 'placement' => 'prepend'))
        ));
        $this->addElement($logar);
    }

}
