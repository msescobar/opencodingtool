<?php

class Sistema_Acl extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $front = Zend_Controller_Front::getInstance();
        if ($front->getDispatcher()->isDispatchable($request)) {

            $acl = new Zend_Acl();
            //Inserindo os papeis do sistemas
            $acl->addRole(new Zend_Acl_Role('admin'));
            $acl->addRole(new Zend_Acl_Role('avaliador'));
            $acl->addRole(new Zend_Acl_Role('codificador'));
            $acl->addRole(new Zend_Acl_Role('ambos'));

            //Disponibilizando os recursos do sistema
            $acl->add(new Zend_Acl_Resource('index'));
            $acl->add(new Zend_Acl_Resource('admin'));
            $acl->add(new Zend_Acl_Resource('admin-avaliador'));
            $acl->add(new Zend_Acl_Resource('admin-codificador'));
            $acl->add(new Zend_Acl_Resource('error'));
            $acl->add(new Zend_Acl_Resource('projeto'));
            $acl->add(new Zend_Acl_Resource('avaliacao'));
            $acl->add(new Zend_Acl_Resource('discente'));
            $acl->add(new Zend_Acl_Resource('usuario'));
            $acl->add(new Zend_Acl_Resource('codigo'));
            $acl->add(new Zend_Acl_Resource('trecho'));
            $acl->add(new Zend_Acl_Resource('grupo-projeto'));
            $acl->add(new Zend_Acl_Resource('grupo-codigo'));
            $acl->add(new Zend_Acl_Resource('arquivo'));
            $acl->add(new Zend_Acl_Resource('codificador-trecho'));
            $acl->add(new Zend_Acl_Resource('grupocodificadorprojeto'));
            $acl->add(new Zend_Acl_Resource('grupocodigohascodigo'));
            $acl->add(new Zend_Acl_Resource('administrador'));
            $acl->add(new Zend_Acl_Resource('criar-conta'));
            $acl->add(new Zend_Acl_Resource('selecionar'));
            
//            $acl->allow(null, 'selecionar');
            $acl->allow(null, 'criar-conta');
            $acl->allow(null, 'grupocodificadorprojeto');
            $acl->allow(null, 'admin', array('sair'));
            $acl->allow(null, 'usuario', array('nova-senha', 'redefinirsenha', 'alterar-senha'));
//            $acl->allow(null, 'codigo');

            $acl->allow('admin', 'administrador');
            $acl->allow('admin', 'usuario');
            
            $acl->allow(array('ambos', 'avaliador'), 'selecionar');
            $acl->allow(array('ambos', 'avaliador'), 'admin');
            $acl->allow(array('ambos', 'avaliador'), 'trecho');
            $acl->allow(array('ambos', 'avaliador'), 'codigo');
            $acl->allow(array('ambos', 'avaliador'), 'avaliacao');
            $acl->allow(array('ambos', 'avaliador'), 'usuario');
            $acl->allow(array('ambos', 'avaliador'), 'admin-avaliador');
            $acl->allow(array('ambos', 'avaliador'), 'projeto');
            $acl->allow(array('ambos', 'avaliador'), 'grupo-projeto');
            $acl->allow(array('ambos', 'avaliador'), 'grupo-codigo');
            $acl->allow(array('ambos', 'avaliador'), 'arquivo');
            $acl->allow(array('ambos', 'avaliador'), 'codificador-trecho');
            $acl->allow(array('ambos', 'avaliador'), 'grupocodigohascodigo');
            
            $acl->allow(array('ambos', 'codificador'), 'selecionar');
            $acl->allow(array('ambos', 'codificador'), 'index');
            $acl->allow(array('ambos', 'codificador'), 'admin-codificador');
            $acl->allow(array('ambos', 'codificador'), 'codigo');
            $acl->allow(array('ambos', 'codificador'), 'grupocodificadorprojeto');
            $acl->allow(array('ambos', 'codificador'), 'trecho');
            $acl->allow(array('ambos', 'codificador'), 'admin');
//            $acl->allow('codificador', 'usuario', array('getAdapterPersonalizado'));

            $auth = Zend_Auth::getInstance();
            
            if ($auth->hasIdentity()) {
                $identity = $auth->getIdentity();
                
                $DbTablePerfil = new Application_Model_DbTable_Perfil();
                $perfil = $DbTablePerfil->getPerfilPorId($identity->idperfil);
                $role = $perfil->getDescricao();
                
            } else {
                $role = null;
            }
            
            $controller = $request->controller;
            $action = $request->action;

            if (!$acl->isAllowed($role, $controller, $action)) {
                if ($role == null) {
                    $request->setControllerName('index');
                    $request->setActionName('index');
                } else {
                    $request->setControllerName('login');
                    $request->setActionName('acesso-negado');
                }
            } else {
                
            }
        } else {
            $request->setControllerName('error');
            $request->setActionName('url-nao-encontrada');
        }
    }

}
